#include <SoftwareSerial.h>

//#define analog_pin 2 //  PA1 of the ATtiny412 -SENSE
//#define tx_pin 3  //     PA2 of the ATtiny412 -STEP

//#define sense1 9 //  PA2 of the ATtiny1614 -SENSE1
//#define sense2 10 //  PA3 of the ATtiny1614 -SENSE2
//#define sense3 8 //  PA1 of the ATtiny1614 -SENSE3
//#define sense4 1 //  PA5 of the ATtiny1614 -SENSE4
//#define sense5 3 //  PA7 of the ATtiny1614 -SENSE5
//#define sense6 2 //  PA6 of the ATtiny1614 -SENSE6
//#define STEP 0  //     PA4 of the ATtiny1614 -STEP


long result[9];
int sense[]={9,10,8,1,3,2};
long noise0,noise1,noise2,noise3,noise4,noise5,noise6,noise7,noise8;
SoftwareSerial mySerial(6,7); //RX_MAG TX_MAG PB1,PB0 of ATtiny1614

void setup() {
  pinMode(sense[3],OUTPUT);
  pinMode(sense[4],OUTPUT);
  pinMode(sense[5],OUTPUT);

  int thres=100000;
  for(int i =0; i<1000; i++);
  {
   long n0=tx_rx(sense[3],sense[0]);
   if(n0<thres);
      noise0+=n0;

    long n1=tx_rx(sense[4],sense[0]);
    if(n1<thres);
      noise1+=n1;
      
    long n2=tx_rx(sense[5],sense[0]);
    if(n2<thres);
      noise2+=n2;

    long n3=tx_rx(sense[3],sense[1]);
    if(n3<thres);
      noise3+=n3;
      
    long n4=tx_rx(sense[4],sense[1]);
    if(n4<thres);
      noise4+=n4;
      
    long n5=tx_rx(sense[5],sense[1]);
    if(n5<thres);
      noise5+=n5;

    long n6=tx_rx(sense[3],sense[2]);
    if(n6<thres);
      noise6+=n6; 

    long n7=tx_rx(sense[4],sense[2]);
    if(n7<thres);
      noise7+=n7; 

    long n8=tx_rx(sense[5],sense[2]);
    if(n8<thres);
      noise8+=n8; 
      
    delay(100);
  }


  
  Serial.begin(115200);
  
  mySerial.begin(115200);  // set the data rate for the SoftwareSerial port

}


long tx_rx(int stepp, int sense){         //Function to execute rx_tx algorithm and return a value
                      //that depends on coupling of two electrodes.
                      //Value returned is a long integer.
  int read_high;
  int read_low;
  int diff;
  long int sum;
  int N_samples = 100;    //Number of samples to take.  Larger number slows it down, but reduces scatter.
  float read_high_adc, read_low_adc;
  sum = 0;

  for (int i = 0; i < N_samples; i++){
   digitalWrite(stepp,HIGH);              //Step the voltage high on conductor 1.
   read_high = analogRead(sense);        //Measure response of conductor 2.
   read_high_adc=read_high*(5.0/1023.0);
   delayMicroseconds(100);            //Delay to reach steady state.
   
   digitalWrite(stepp,LOW);               //Step the voltage to zero on conductor 1.
   read_low = analogRead(sense);         //Measure response of conductor 2.
   read_low_adc=read_low*(5.0/1023.0);
   //diff = read_high_adc - read_low_adc;       //desired answer is the difference between high and low.
   diff=read_high-read_low;
   sum += diff;                       //Sums up N_samples of these measurements.
   }
  return sum;
}                         //End of tx_rx function.


void loop() {
  //   0  1  2
  //   1  
  //   2
    result[0]= tx_rx(sense[3],sense[0])-noise0; //blue //0,0
    result[1]= tx_rx(sense[4],sense[0])-noise1; //red //0,1
    result[2]= tx_rx(sense[5],sense[0])-noise2; //green //0,2
    result[3]= tx_rx(sense[3],sense[1])-noise3; //orange //1,0
    result[4]= tx_rx(sense[4],sense[1])-noise4; //purple 1,1
    result[5]= tx_rx(sense[5],sense[1])-noise5; //grey 1,2
    result[6]= tx_rx(sense[3],sense[2])-noise6; //sky 2,0
    result[7]= tx_rx(sense[4],sense[2])-noise7; //black 2,1
    result[8]= tx_rx(sense[5],sense[2])-noise8; //dark blue 2,2
    
    for(int i=0; i<9;i++)
    {
      Serial.print(result[i]);
      Serial.write('\t');
    }
    Serial.println("\n");

//    for(int i=0;i<9;i++)
//    {
//      if(result[i]>1000)
//        Serial.print(i);
//    }
//     Serial.println("\n");
//  if(result[0]>100){
//    mySerial.write(192);//1100 0000
//    //mySerial.write(64); //0100 0000
//  }
//  else{
//    mySerial.write(128);//1000 0000
//    //mySerial.write(0); //0000 0000
//  }
  
  delayMicroseconds(100);
}
