//Fab Academy 2021 - Fab Lab LeÃ³n
//Step Response
//Adrianino
//ATtiny1614

//Modified by Dinan Yan
//Fab Academy 2021
//Step Response
//ATtiny412

import processing.serial.*;

float sensorValue; //variable for the serial data
Serial myPort;

void setup() {  //as dynamic/setup function called initially, only once
  size(1024, 200);// is the window (1024=sensor max. value)
  myPort = new Serial(this, "/dev/cu.usbserial-FTBVM4SY", 115200); // serial port
  background(255);  //set background white
  
}

void draw() {  //draw function loops 
  
  noStroke(); // outline
  fill(255,0,0,20); // color inside
  rect(0, 0, sensorValue, height); //position and size
  
  fill(255,70);
  rect(sensorValue, 0, width-sensorValue, height);
  
  println(sensorValue);
  fill(0,0,0);// these are the colors inside
  text(sensorValue + " " , sensorValue, height/2);
  textSize(32);
    
}

void serialEvent(Serial myPort) { // sketch read the serial data
  String inString = myPort.readStringUntil('\n');
  if (inString != null) {
    inString = trim(inString);
    float[] values = float(split(inString, ","));
    if (values.length >=1) {
      sensorValue = values[0]; //first value in the list
    }
  }
}
