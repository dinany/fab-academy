+++
title="Computer-Controlled Machining"
lead = "Just a table"
+++

**Group**:

- do your lab's safety training
- test runout, alignment, speeds, feeds, materials, and toolpaths for your machine

**Individual**:

- make (design+mill+assemble) something big (~meter-scale)

# Individual Assignment

I was inspired by the tensegrity struture Neil mentioned in global lecture so I made a floating table this week.

{{<image-responsive src="media/table.jpg" alt="Table">}}
<br />

##### Fusion 360

I designed the table in Fusion 360.

{{<image-responsive src="media/tablemodel.jpg" alt="Table Model">}}
<br />

I downloaded and intalled dogbone addin from [github](https://github.com/DVE2000/Dogbone). I set the tool diameter to 8mm, which is the tool size I plan to use in CNC machine.

{{<image-responsive src="media/dogbone.jpg" alt="Dogbone">}}
<br />

To move the parts onto stock and align properly, I selected flip and planer under Assemble-joint.

{{<image-responsive src="media/flip.jpg" alt="Flip">}}
<br />
{{<image-responsive src="media/planer.jpg" alt="Planer">}}
<br />

And I created tool path for CNC milling in Manufacture workspace.
I selected the bottom left corner as origin and also set up a tool template for 8mm flat tool with two flutes.

{{<image-responsive src="media/toolsetup1.jpg" alt="Tool setup 1">}}
<br />

At first, I mistakenly put 16000 mm/min cutting feedrate because I put the 0.4 mm feed per teeth of MDF. But I used plywood as material instead of MDF. Fortunately, Solomon found the value wrong in advance and adjusted the speed down to 50% before cutting.

He also introduced this website to calculate the feedrate (http://www.labbers.com/feedratecalculator/).

{{<image-responsive src="media/toolsetup2.jpg" alt="Tool setup 2">}}
<br />

Below is the form in Fablab for feedrate calculation.

{{<image-responsive src="media/form.jpg" alt="Feedrate Form">}}
<br />

Then I clicked 2D contour. For the coutour selection under Geometry section, I chose all the paths on the lowest level and double checked the red arrows were outside the paths.

{{<image-responsive src="media/contour.jpg" alt="Contour">}}
<br />

I added tabs to fix the parts while cutting. However, it turned out that those tabs were a bit too small. So I should increase the width and height next time.

{{<image-responsive src="media/tab.jpg" alt="Tab">}}
<br />

Under Passes section, I changed the max roughing stepdown to half of the tool size.

{{<image-responsive src="media/mdepth.jpg" alt="Multiple Depth">}}
<br />

I added 0.5mm axial stock to leave but I should have more since it turned out that some parts were not fully cut through.

{{<image-responsive src="media/axialstock.jpg" alt="Axial Stock">}}
<br />

In post process, I selected Mach3Mill as the post processor.

{{<image-responsive src="media/post.jpg" alt="Post Process">}}
<br />

Last but not least, I simulated the milling process to check if anything was wrong.

{{<image-responsive src="media/simulate.jpg" alt="Simulate">}}
<br />

##### CNC Milling Recontech 1312

I used 8mm tool for CNC milling.

{{<image-responsive src="media/8mm.jpg" alt="8mm Tool">}}
<br />

Also selected the corresponding shaft.

{{<image-responsive src="media/shaft.jpg" alt="Shaft">}}
<br />

Here is the Mach3 interface.

{{<image-responsive src="media/mach3.jpg" alt="Mach3">}}
<br />

Here's a footage of cutting:

{{<video>}}media/cnc.mp4{{</video >}}

The direction to loose the shaft is the opposite with the MDX-40 machine.

{{<image-responsive src="media/wrentch.jpg" alt="Wrentch">}}
<br />

As I mentioned above, the parts were not fully cut through due to the uneven height of the stock.

{{<image-responsive src="media/notCutThrough.jpg" alt="Not Cut THrough">}}
<br />

I forgot to shrink the gap in CAD. Because of the material loss in cutting and sanding, the gap was too big to fit perfect.

{{<image-responsive src="media/kerf.jpg" alt="Kerf">}}
<br />

So I have to use glue.

{{<image-responsive src="media/glue.jpg" alt="Glue">}}
<br />

Fablab provided two kinds of wires - ropes and translucent fishing lines. For better creating an illusion that the table is floating in the air, I chose the fishing lines. One thread can handle 4.56kg and I used four threads on each spot.

{{<image-responsive src="media/wires.jpg" alt="Wires">}}
<br />

For fixtures, I 3D printed them with the design file Kris made.

We tested the max weight the rope and fixture can bear.

{{<image-responsive src="media/weight.jpg" alt="Weight">}}
<br />

Kris doing the test:

{{<video>}}media/kris.mp4{{</video >}}

It was time-consuming to assemble because it was difficult to equally tighten each wire by hands. I didn't manage to tighten them in the end so the table is quite fragile right now. But at least it can bear the weight of one phone since I've tested it.

Just a table here:

{{<image-responsive src="media/table.jpg" alt="Table">}}
<br />

# Group Assignment

I am in the group with Onni Eriksson and Ruo-xuan Wu. Please find our group assignment [here](https://onni-innovations.gitlab.io/fab-academy/assignments/assignment-08-group/).

# Download

[Floating Table](media/table-8mm-upcut.tap) tap file for CNC milling

Fusion 360 3D model

{{<fusion src="https://aalto180.autodesk360.com/g/shares/SH56a43QTfd62c1cd9687cd225d1ab70d361">}}
