+++
title="Computer-Controlled Cutting"
lead = "Explore vinyl cutter and laser cutter"
+++

# Individual Assignment

## 1. Vinyl Cutter

I made two stickers for my snowboard - "SHRED" and "SWITCH IT UP". I am practicing switch riding this season but still not comfortable with it. So I put "SWITCH IT UP" on the tail to push myself out of the comfort zone 🏂

{{<image-responsive src="shred_resize.jpg" alt="Shred">}}

{{<image-responsive src="switch_resize.jpg" alt="Switch">}}

{{<image-responsive src="burton_resize.jpg" alt="Burton">}}

{{<image-responsive src="sticker_resize.jpg" alt="Sticker">}}

I created the graphics in Illustrator. Detailed steps are posted under [Computer-Aided Design page](https://dinany.gitlab.io/fab-academy/assignments/computer-aided-design/). For the "SHRED" sticker, I cut two pieces with different color and layered them together.

## 2. Laser Cutter

Making stiff plywood bended seems interesting. So I experimented with living hinge and made a container for my makeup brushes and lipsticks.

{{<image-responsive src="fold.jpg" alt="Fold">}}
{{<image-responsive src="container.jpg" alt="Container">}}
<br />

At first I set the horizontal gap between lines to be 2mm. It turned out that the piece could bend but not flexible enough to fold as a cylinder. Then I decreased it to be 1mm and it works quite well.

{{<image-responsive src="unfold.jpg" alt="Unfold">}}
{{<image-responsive src="compare.jpg" alt="Compare">}}
<br />

Here is the 3D model I created in Fusion.

{{<image-responsive src="3d.jpg" alt="3d">}}
<br />

I used the sheet metal feature in fusion because it can easily unfold and refold 3D objects.

{{<image-responsive src="surface.jpg" alt="Surface">}}
<br />

I used parametric design and set most dimensions in parameter interface. After I did test piece, I adjusted dimensions easily because of parametric setting.

{{<image-responsive src="parameter.jpg" alt="Parameter">}}
<br />

I exported both the sketch and projection of unfolded surface and edited a bit in Illustrator to combine them. It wouldn't work well in the laser cutter if I just exported the projection of unfolded cylinder surface. Because the "line" on the surface is actually a tiny cut formed by two lines. The laser would cut the same place twice and it would be nasty.

{{<image-responsive src="hinge.jpg" alt="Hinge">}}

The biggest challenge in this project was converting files from Illustrator to CorelDraw/Inkscape since the computer in fablab couldn't open ai file for some reason. I exported svg from Illustrator on my local computer and opened it in CorelDraw and Inkscape in lab. However, both CorelDraw and Inkscape resized my svg file automatically and messed dimensions. It was so time consuming for me to redo everything with CorelDraw since I never used it before.

Now I found the cause and solution online. Svg file is xml based and when I opened it with Sublime, I saw the width and heigh are in px. So it uses pixel instead of mm or inch as unit. All three vector softwares have different default DPI. That's why the scale in the same file changes among those three.

- Illustrator: 72 DPI
- Inkscape: 96 DPI
- Not sure about CorelDraw

One solution is that instead of opening the svg file directly by double clicking, I created a new Inkscape file and drag the svg into it. Then the following window popped out. I set the DPI to be the same as Illustrator and the scale now would be correct.

{{<image-responsive src="dpi.jpg" alt="DPI">}}
<br />

Here are videos of laser cutting and assembling:

{{<video>}}lasercut.mp4{{</video >}}
{{<video>}}assemble.mp4{{</video >}}

Fusion 360 3D model

{{<fusion src="https://aalto180.autodesk360.com/g/shares/SH56a43QTfd62c1cd9689dac7a6cec24601d?viewState=NoIgbgDAdAjCA0IDeAdEAXAngBwKZoC40ARXAZwEsBzAOzXjQEMyzd1C0A2TgZkYA4ALACMIAWk64A7AGMxggJwRBYxoICs-MQCZlPbev0ATRhBhoAviAC6QA?mode=embed">}}

# Group Assignment

I am in the group with Eero Pitkänen and Onni Eriksson. Please find our group assignment [here](https://eeropic.gitlab.io/fablab-doc/assignments/assignment-04-group/).

# Download

Download files: [living-hinge.stl/living-hinge.svg](computer-controlled-cutting.zip)
