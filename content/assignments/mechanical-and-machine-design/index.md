+++
title="Mechanical and Machine Design"
lead = "Make a punch robot"
+++

## Group assignment

- design a machine that includes mechanism+actuation+automation
- build the mechanical parts and operate it manually
- document the group project and your individual contribution

I am in a group with Krisjanis Rijnieks and Onni Eriksson. Here is our [GitLab repo](https://gitlab.com/aaltofablab/machine-building-2021-group-b).

We plan to build a punch robot. I am responsible for actuation part.

I milled the stepper shield. I followed the [3 axis stepper motor Arduino Shield design](https://github.com/jw4rd/stepper/) and redrew everything from scratch in KiCad.

For the schematic design, instead of using [TMC 2208 Stepper Driver Board](https://www.trinamic.com/fileadmin/assets/Products/ICs_Documents/TMC220x_TMC2224_datasheet_Rev1.09.pdf) which we have in fablab, I used [Pololu A4988](https://www.pololu.com/product/1182) in the kiCad libary because they have same dimensions and similar pinout.

{{<image-responsive src="media/PololuA4988.jpg" alt="PololuA4988">}}
<br />
{{<image-responsive src="media/TMC2208.jpg" alt="TMC2208">}}
<br />

Comparsion of two breakouts:

| Polulu A4988 | ------------ | TMC 2208 |
| ------------ | :----------: | -------- |
| MS3          |              | UART     |
| Reset        |              | PDN      |
| Sleep        |              | CLK      |

<br />

Here's my schematic design:

{{<image-responsive src="media/schematic.jpg" alt="Schematic">}}
<br />

Then I routed tracks in pcbnew. But I forgot to add two holes for the power jack.

{{<image-responsive src="media/routeTracks.jpg" alt="Route Tracks">}}
<br />

Finally I milled the board using MDX-40. I used 0.4mm bit for milling, 0.8mm bit for drilling and 1.0mm bit for cutting the outline.

{{<image-responsive src="media/shieldMilling.jpg" alt="Shield">}}
<br />

To save time, we used the board Kris made.

{{<image-responsive src="media/unplugBoard2.jpg" alt="UnplugBoard2">}}
<br />

{{<image-responsive src="media/attachBoard.jpg" alt="AttachBoard">}}
<br />

To control the stepper motor, we used GRBL + Universal Gcode Sender.

### GRBL

Install [GRBL](https://github.com/gnea/grbl) to Arduino library.

{{<image-responsive src="media/installGrbl.jpg" alt="Install GRBL">}}
<br />

Modified cpu_map.h to fit Arduino Uno pin location.

{{<image-responsive src="media/pin.jpg" alt="Pins">}}
<br />

{{<image-responsive src="media/cpumap.jpg" alt="cpu_map.h">}}
<br />

Upload and compile.

{{<image-responsive src="media/upload.jpg" alt="Upload">}}
<br />

I opened the serial monitor to check the connection and it works!

{{<image-responsive src="media/serial.jpg" alt="Serial">}}
<br />

### Universal Gcode Sender

Download [Universal Gcode Sender](https://winder.github.io/ugs_website/)

Here is the user interface.

{{<image-responsive src="media/ugs.jpg" alt="UGS">}}
<br />

step/mm can be set by the command.

{{<image-responsive src="media/ugs2.jpg" alt="UGS2">}}
<br />

However, the motor didn't move when I hit x+. The connection was good though. After debugging under the help with Kris, we found that it was caused by the change of pins in cpu_map.h because Kris designed the board with the default pin setting.

I changed it back to default.

{{<image-responsive src="media/pin_fixed.jpg" alt="Pin fixed">}}
<br />

And now it works!

{{<video>}}media/motor.mp4{{</video >}}

Hero shot!

{{<image-responsive src="media/hero.jpg" alt="Hero shot">}}
<br />
