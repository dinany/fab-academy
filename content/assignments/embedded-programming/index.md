+++
title="Embedded Programming"
lead = "..."
+++

**Group**:

- compare the performance and development workflows for other architectures

**Individual**:

- read a microcontroller data sheet
- program your board to do something, with as many different programming languages and programming environments as possible

# Individual Assignment

Kris helped me mill this double-sided USB to FTDI FT230XS Programmer with MDX-40. Please find design files on Aalto Fablab GitLab page [here](https://gitlab.com/aaltofablab/usb-ftdi-ft230xs).

{{<image-responsive src="media/pcb1.jpg" alt="PCB1">}}
<br />

{{<image-responsive src="media/pcb2.jpg" alt="PCB2">}}
<br />

Here is the list of electronics components needed.

{{<image-responsive src="media/ftdiList.jpg" alt="FTDI List">}}
<br />

However, the FTDI programmer doesn't work properly. It couldn't be detected when I tried to connect it to Arduino.

{{<image-responsive src="media/uselessFTDI.jpg" alt="FTDI programmer">}}
<br />

Plus, I broke the Hello board by accidentally tearing off the FTDI header with the copper layer.

Corpse..

{{<image-responsive src="media/corpse.jpg" alt="Corpse">}}
<br />

Great, I had to mill and solder a Hello board AGAIN.

To avoid similar tragedy, I soldered FTDI header vertically on the board. I borrowed FTDI cable from fablab.

{{<image-responsive src="media/test.jpg" alt="test">}}
<br />

Test with echo code from Neil.

{{<image-responsive src="media/echo1.jpg" alt="Echo1">}}
{{<image-responsive src="media/echo2.jpg" alt="Echo2">}}
<br />

However the led didn't light up when I tried blink code because I switched the anode and cathode sides. I didn't know those leds were polarized but I always got the direction right before this week. Hope I can bring that luck when I go to casino.

Ranjit and I checked leds with different color in the lab with multimeter to figure out which side is negative. They all have some marks.

Here is the result:

| COLOR  | ------------ | MARK                               | POLARITY |
| ------ | :----------: | ---------------------------------- | -------- |
| White  |              | Black line                         | "-"      |
| Green  |              | Green line                         | "-"      |
| Blue   |              | Thicker line/Back side green squre | "-"      |
| Red    |              | Green line                         | "-"      |
| Orange |              | Green line                         | "-"      |

### PlatformIO

I tried another framework which is [PlatformIO](https://platformio.org). I installed it inside Visual Studio Code as an extension. Here is a quick [tutorial](https://docs.platformio.org/en/latest/integration/ide/vscode.html#quick-start).

{{<image-responsive src="media/platformIO.jpg" alt="PlatformIO">}}
<br />

{{<image-responsive src="media/wizard.jpg" alt="Wizard">}}
<br />

I created a new project and pasted the blink code there.

{{<image-responsive src="media/blink.jpg" alt="Blink Code">}}
<br />

Not like Arduino, the port is selected automatically but sometimes it's not correct. To manually select, I went to "Device" and copied the name of the right port.

{{<image-responsive src="media/port1.jpg" alt="Port1">}}
<br />

Then I added the follwoing to platformio.ini file.

```go
upload_port = /dev/cu.usbserial-D3072FCL
```

However when I compiled and uploaded to hello board, it gave me this error:

```go
avrdude: jtagmkII_getsync(): bad response to sign-on command: Unknown JTAG ICE mkII result code 0x01
```

I figured out that it can be fixed by installing the [pyupdi driver][https://github.com/mraardvark/pyupdi]

I checked my python and pip version.

{{<image-responsive src="media/pythonVersion.jpg" alt="Python version">}}
<br />

Installed pyupdi directly in PlatformIO.

{{<image-responsive src="media/pyupdi.jpg" alt="Pyupdi">}}
<br />

I followed instrucion section of [Upload using pyupdi](https://docs.platformio.org/en/latest/platforms/atmelmegaavr.html#upload-using-pyupdi) and configured the platformio.ini file as following:

```go
[env:ATtiny412]
platform = atmelmegaavr
board = ATtiny412
framework = arduino
upload_port=/dev/cu.usbserial-D3072FCL
upload_speed = 115200
upload_flags =
    -d
    tiny412
    -c
    $UPLOAD_PORT
    -b
    $UPLOAD_SPEED
upload_command = pyupdi $UPLOAD_FLAGS -f $SOURCE
```

Finally it succeeded.

{{<image-responsive src="media/success.jpg" alt="Success">}}
<br />

{{<video>}}media/blink.mp4{{</video >}}
<br />

### ATtiny412 Datasheet

I skimmed through [ATtiny412 Datasheet](http://academy.cba.mit.edu/classes/embedded_programming/t412/ATtiny212-412-DataSheet-DS40001911C.pdf).

ATtiny412 is member of the tinyAVR 1-seriesof microcontrollers. It uses the AVR processor, running at up to 20MHz, with 4 KB Flash, 256 bytes of SRAM, 128 bytes of EEPROM in an 8-pin package. The AVR CPU uses a Harvard architecture with separate buses for program and data.

Pinout:

{{<image-responsive src="media/pinout.jpg" alt="Pinout">}}
<br />

**VDD**: Power Supply Voltage

**EXTCLK**: External Clock. The External Clock is taken directly from the pin. This GPIO (General Purpose Input/Output) pin is automatically configured for EXTCLK if any peripheral is requesting this clock.

**USART**: Universal Synchronous and Asynchronous Receiver and Transmitter. It is a fast and flexible serial communication module. The USART supports full duplex communication, asynchronous and synchronous operation, and one-wire configurations. The USART can be set in SPI Master mode and used for SPI communication.

The USART uses three communication lines for data transfer:

- RxD for receiving
- TxD for transmitting
- XCK for the transmission clock in synchronous operation

**SPI**: Serial Peripheral Interface. It is a a high-speed synchronous data transfer interface using three or four pins. It allows full duplex communication between an AVR device and peripheral devices or between several microcontrollers. The SPI peripheral can be configured as either Master or Slave.

### Write original code

I programmed the helloboard to make the LED lighten up when I press the button.

```go
#define LED 4 //PA3
#define BUTTON 3 //PA2

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);

  Serial.begin(115200);
  digitalWrite(LED, LOW);
}

void loop() {
  int onOrOff=!digitalRead(BUTTON);
  Serial.println(onOrOff);

  if(onOrOff){
    digitalWrite(LED, HIGH);
  }else{
    digitalWrite(LED, LOW);
  }

  delay(100);

}
```

Demo:
{{<video>}}media/demo.mp4{{</video >}}
<br />
