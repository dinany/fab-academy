+++
title="Input Devices"
lead = "Experiment with step response"
+++

**Assignment**:

- measure something: add a sensor to a microcontroller board that you have designed and read it
- probe an input device's analog levels and digital signals

Since I plan to make a touch sreen as part of my final project, I decide to dive deeper into step response for this week's assignment.

To get started, I followed Neil's design - [hello.txrx.45](http://academy.cba.mit.edu/classes/input_devices/step/hello.txrx.45.png) with ATtiny45.

Starting from schematic part in kicad:

{{<image-responsive src="media/45Schem.jpg" alt="t45 Schematic">}}
<br />

{{<image-responsive src="media/45pcbview1.jpg" alt="45pcbview1">}}
<br />

It's single-sided board.

{{<image-responsive src="media/45pcbview2.jpg" alt="45pcbview2">}}
<br />

I decided to experiment with CopperCAM for toolpath so I exported the gerber file.

{{<image-responsive src="media/gerber1.jpg" alt="Gerber1">}}
<br />

{{<image-responsive src="media/gerber2.jpg" alt="Gerber2">}}
<br />

Specify the thickness to be 1.6mm.

{{<image-responsive src="media/CM1.jpg" alt="CM1">}}
<br />

{{<image-responsive src="media/CM2.jpg" alt="CM2">}}
<br />

Because both computers beside milling machines were in use by other students, I installed CopperCAM on another computer. So I had to add tools to the library.

{{<image-responsive src="media/CM3.jpg" alt="CM3">}}
<br />

Select tools:

{{<image-responsive src="media/CM4.jpg" alt="CM4">}}
<br />

I set offset to 4.

{{<image-responsive src="media/CM5.jpg" alt="CM5">}}
<br />

{{<image-responsive src="media/CM6.jpg" alt="CM6">}}
<br />

Output toolpath:

{{<image-responsive src="media/CM7.jpg" alt="CM7">}}
<br />

All the electronic components are listed here.

{{<image-responsive src="media/t45List.jpg" alt="45 List">}}
<br />

Here is the board I made:

{{<image-responsive src="media/45.jpg" alt="T45 Board">}}
<br />

However, this board is designed to work with ISP programmer, which is an old way to program.

Therefore, I designed a new board with ATtiny412 so that it can be programmed with my UPDI programmer.

{{<image-responsive src="media/412schem.jpg" alt="412 Schematic">}}
<br />

{{<image-responsive src="media/412pcbview.jpg" alt="412 pcbview">}}
<br />

{{<image-responsive src="media/t412List.jpg" alt="412 List">}}
<br />

{{<image-responsive src="media/412.jpg" alt="T412 Board">}}
<br />

But I noticed that two resistors were missing comparing the board Neil designed.

{{<image-responsive src="media/miss.jpg" alt="1M R">}}
<br />

What are those two 1M ohm resistors for?

To figuire out the answer, I started from the basics of capacitive touch sensor and step response.

### Capacitive Touch Sensor

I found a nice introduction [here](https://www.allaboutcircuits.com/technical-articles/introduction-to-capacitive-touch-sensing/) about Capacitive touch sensor.

Capacitive touch sensors includes two general categories:

1. the mutual-capacitance configuration: sensing capacitor is composed of two terminals that function as emitting and receiving electrodes, is preferred for touch-sensitive displays.
2. the self-capacitance configuration: one terminal of the sensing capacitor is connected to ground. It is suitable for a touch-sensitive button, slider, or wheel.

For my final project, I tend to go by the mutual-capacitance configuration.

### Step Response

Step response in general is time delay or Time Constant which represents the time response of the circuit when an input step voltage or signal is applied in RC(Resistor-Capacitor) Circuit. If a resistor is connected in series with the capacitor forming an RC circuit, the capacitor will charge up gradually through the resistor until the voltage across it reaches that of the supply voltage. I found a detailed explanation [here](https://www.electronics-tutorials.ws/rc/rc_1.html).

### Circuit Design for Capacitive Touch Sensing

Below is an interesting point I found in an [article](https://www.allaboutcircuits.com/technical-articles/circuits-and-techniques-for-implementing-capacitive-touch-sensing/):

"_We can use a general-purpose input/output pin (configured as an output) to charge the sensor cap up to the logic-high voltage. Next, we need the capacitor to discharge through the large resistor. It’s important to understand that you cannot simply switch the output state to logic low. An I/O pin configured as an output will drive a logic-low signal, i.e., it will provide the output with a low-impedance connection to the ground node. Thus, the capacitor would discharge rapidly through this low impedance—so rapidly that the microcontroller could not detect the subtle timing variations created by small changes in capacitance. What we need here is a high-impedance pin that will force almost all of the current to discharge through the resistor, and this can be accomplished by configuring the pin as an input. So first you set the pin as a logic-high output, then the discharge phase is initiated by changing the pin to an input_."

[Arduino](https://playground.arduino.cc/Main/CapacitiveSensor/) also has a nice introduction.

Capacitive Sensing toggles a microcontroller send pin to a new state and then waits for the receive pin to change to the same state as the send pin. A variable is incremented inside a while loop to time the receive pin's state change.

The circuit includes a medium to high value (100 kilohm - 50 megohm) resistor between the send pin and the receive (sensor) pin. The receive pin is the sensor terminal.

The delay between the send pin changing and the receive pin changing is determined by an RC time constant, defined by R \* C.

- Use a 1 megohm resistor (or less maybe) for absolute touch to activate.
- With a 10 megohm resistor the sensor will start to respond 4-6 inches away.
- With a 40 megohm resistor the sensor will start to respond 12-24 inches away (dependent on the foil size).
- One tradeoff with larger resistors is that the sensor's increased sensitivity means that it is slower. Also if the sensor is exposed metal, it is possible that the send pin will never be able to force a change in the receive (sensor) pin, and the sensor will timeout.
- Also experiment with small capacitors (100 pF - .01 uF) to ground, on the sense pin. They improve stability of the sensor.

### What are those two 1M ohm resistors for?

[Robert Hart](https://roberthart56.github.io/SCFAB/SC_lab/Sensors/tx_rx_sensors/)
said two resistors (1 MOhm or greater) can be placed between 5V and GND, with the signal connected between them so that the steady-state voltage is 2.5 Volts.

It is actually a parallel RC circuit as below. Those are screenshots of a Youtube tutorial [here](https://youtu.be/4I5hswA45CM)

{{<image-responsive src="media/RC1.jpg" alt="RC1">}}
<br />

{{<image-responsive src="media/RC2.jpg" alt="RC2">}}
<br />

With the formular above, V2 = I2 x R2 = V/(R1+R2) \* R2 = 5/2 = 2.5v

BUT WHY DOES IT NEED TO BE 2.5V? I haven't figured out.

I added two 1M resistors to the board.

{{<image-responsive src="media/jumper.jpg" alt="Jumper">}}
<br />

I connected it to the copper foils.

{{<image-responsive src="media/demo1.jpg" alt="Demo1">}}
<br />

### Programming

I learned from and modified Robert Hart's and Adrián Torres's code.

#### Arduino

I edited pin number based on ATtiny412 pinout.

{{<image-responsive src="../electronics-design/media/ATtiny_x12.gif" alt="RC1">}}
<br />

I also remapped the values of the two copper plates.

[CODE](media/step_response_t412.ino)

```go
//tx_rx03  Robert Hart Mar 2019.
//https://roberthart56.github.io/SCFAB/SC_lab/Sensors/tx_rx_sensors/index.html

//Modified by AdriÃ¡n Torres OmaÃ±a
//Fab Academy 2021
//Step Response TX, RX
//Adrianino
//ATtiny1614

//Modified by Dinan Yan
//Fab Academy 2021
//Step Response
//ATtiny412

//  Program to use transmit-receive across space between two conductors.
//  One conductor attached to digital pin, another to analog pin.
//
//  This program has a function "tx_rx() which returns the value in a long integer.
//
//  Optionally, two resistors (1 MOhm or greater) can be placed between 5V and GND, with
//  the signal connected between them so that the steady-state voltage is 2.5 Volts.
//
//  Signal varies with electric field coupling between conductors, and can
//  be used to measure many things related to position, overlap, and intervening material
//  between the two conductors.
//
//
#define analog_pin 2 //  PA1 of the ATtiny412
#define tx_pin 3  //     PA2 of the ATtiny412

long result;   //variable for the result of the tx_rx measurement.

void setup() {
pinMode(tx_pin,OUTPUT);      //Pin 3 provides the voltage step
Serial.begin(115200);
}


long tx_rx(){         //Function to execute rx_tx algorithm and return a value
                      //that depends on coupling of two electrodes.
                      //Value returned is a long integer.
  int read_high;
  int read_low;
  int diff;
  long int sum;
  int N_samples = 100;    //Number of samples to take.  Larger number slows it down, but reduces scatter.

  sum = 0;

  for (int i = 0; i < N_samples; i++){
   digitalWrite(tx_pin,HIGH);              //Step the voltage high on conductor 1.
   read_high = analogRead(analog_pin);        //Measure response of conductor 2.
   delayMicroseconds(100);            //Delay to reach steady state.
   digitalWrite(tx_pin,LOW);               //Step the voltage to zero on conductor 1.
   read_low = analogRead(analog_pin);         //Measure response of conductor 2.
   diff = read_high - read_low;       //desired answer is the difference between high and low.
 sum += diff;                       //Sums up N_samples of these measurements.
 }
  return sum;
}                         //End of tx_rx function.


void loop() {

result = tx_rx();
result = map(result, 6000, 16000, 0, 1024);  //I recommend mapping the values of the two copper plates, it will depend on their size
Serial.println(result);
delay(100);
}
```

#### Processing

Adrián wrote processing code to visualize the step response.

[CODE](media/step_response_processing.pde)

```go
//Fab Academy 2021 - Fab Lab LeÃ³n
//Step Response
//Adrianino
//ATtiny1614

//Modified by Dinan Yan
//Fab Academy 2021
//Step Response
//ATtiny412

import processing.serial.*;

float sensorValue; //variable for the serial data
Serial myPort;

void setup() {  //as dynamic/setup function called initially, only once
  size(1024, 200);// is the window (1024=sensor max. value)
  myPort = new Serial(this, "/dev/cu.usbserial-FTBVM4SY", 115200); // serial port
  background(255);  //set background white

}

void draw() {  //draw function loops

  noStroke(); // outline
  fill(255,0,0,20); // color inside
  rect(0, 0, sensorValue, height); //position and size

  fill(255,70);
  rect(sensorValue, 0, width-sensorValue, height);

  println(sensorValue);
  fill(0,0,0);// these are the colors inside
  text(sensorValue + " " , sensorValue, height/2);
  textSize(32);

}

void serialEvent(Serial myPort) { // sketch read the serial data
  String inString = myPort.readStringUntil('\n');
  if (inString != null) {
    inString = trim(inString);
    float[] values = float(split(inString, ","));
    if (values.length >=1) {
      sensorValue = values[0]; //first value in the list
    }
  }
}
```

Demo

{{<video>}}media/demo.mp4{{</video >}}
<br />

### Useful Reference

- [Physics Ninja](https://youtu.be/4I5hswA45CM)
- [Arduino Capacitive Sensing](https://playground.arduino.cc/Main/CapacitiveSensor/)
- [Matt Keeter](http://fab.cba.mit.edu/classes/863.11/people/matthew.keeter/multitouch/index.html)
- [Jacopo Di Matteo](http://fab.academany.org/2019/labs/waag/students/jacopo-dimatteo/assignments/week11/)
- [Adrián Torres](https://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html#step)
- [Robert Hart](https://roberthart56.github.io/SCFAB/SC_lab/Sensors/tx_rx_sensors/)

# Download

[step-response-t412-kicad.zip](media/step-response-t412-kicad.zip)
