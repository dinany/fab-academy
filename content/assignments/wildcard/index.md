+++
title="Wildcard"
lead = "Other machines"
+++

**Assignment**:

- Design and produce something with a digital fabrication process
  (incorporating computer-aided design and manufacturing) not covered
  in another assignment, documenting the requirements that your
  assignment meets, and including everything necessary to reproduce
  it.

This week we had tour in Väre to explore other machines.

# Zund

Zund can cut cardboards, fabrics, plastics, etc.

{{<image-responsive src="media/zund.jpg" alt="zund">}}
{{<image-responsive src="media/panel.jpg" alt="panel">}}
<br />

Most commonly used file format is ai.

{{<image-responsive src="media/interface.jpg" alt="interface">}}
<br />
{{<image-responsive src="media/file.jpg" alt="file">}}
<br />

Different tools:

{{<image-responsive src="media/tool.jpg" alt="tool">}}
{{<image-responsive src="media/tool2.jpg" alt="tool2">}}
{{<image-responsive src="media/tool3.jpg" alt="tool3">}}
<br />

Demo:

{{<video>}}media/cardboard.mp4{{</video >}}
{{<video>}}media/plastic.mp4{{</video >}}
<br />

Result:

{{<image-responsive src="media/result.jpg" alt="result">}}
<br />

## Self Experiment

Later, I used Zund to cut and crease cardboard to make a box. I drew the pattern in Illustrator first and imported to Zund. You can find the ai file at the bottom of this page.

{{<image-responsive src="media/cut.jpg" alt="cut">}}
{{<image-responsive src="media/outcome.jpg" alt="outcome">}}
<br />

{{<video2>}}media/zund.mp4{{</video2 >}}
<br />

I designed the box with tabs so that it doesn't need glue at all.

Folded!

{{<image-responsive src="media/box.jpg" alt="box">}}
<br />

# Universal Robot

{{<image-responsive src="media/robot.jpg" alt="robot">}}
<br />

Use hand to visualize X Y Z axis.

{{<image-responsive src="media/axis.jpg" alt="axis">}}
<br />

Interface:

{{<image-responsive src="media/robotInterface.jpg" alt="robotInterface">}}
<br />

3D model:

{{<image-responsive src="media/model.jpg" alt="model">}}
<br />

Use interface to control the robotic arm:
{{<video2>}}media/control.mp4{{</video2>}}
Set waypoint:
{{<video2>}}media/waypoint.mp4{{</video2>}}
Program the robotic arm:
{{<video2>}}media/beforesmooth.mp4{{</video2>}}
Smooth out the movement:
{{<video2>}}media/smooth.mp4{{</video2>}}

# Waterjet

{{<image-responsive src="media/waterjet.jpg" alt="waterjet">}}
<br />

Filling water before cut for safety.

{{<video2>}}media/fillWater.mp4{{</video2>}}

Cutting.

{{<video2>}}media/cut.mp4{{</video2>}}

Pumping.
{{<video2>}}media/pump.mp4{{</video2>}}

Before the last cut, placing toothsticks to avoid the object dropping down.

{{<image-responsive src="media/toothstick.jpg" alt="toothstick">}}
<br />

Here is the outcome:

{{<image-responsive src="media/result2.jpg" alt="result2">}}
<br />
{{<image-responsive src="media/result1.jpg" alt="result1">}}
<br />
{{<image-responsive src="media/result3.jpg" alt="result3">}}
<br />

# Vacuum Forming

Instructions:

{{<image-responsive src="media/instruction1.jpg" alt="instruction1">}}
{{<image-responsive src="media/instruction2.jpg" alt="instruction2">}}
<br />

Control Panel

{{<image-responsive src="media/controlPanel.jpg" alt="controlPanel">}}
<br />

Material dimension and parameter setting

{{<image-responsive src="media/dimension.jpg" alt="dimension">}}
{{<image-responsive src="media/parameter.jpg" alt="parameter">}}
<br />

Lock system

{{<image-responsive src="media/lock.jpg" alt="lock">}}
<br />

Demo

{{<image-responsive src="media/black.jpg" alt="black">}}
{{<image-responsive src="media/vacuumDemo.jpg" alt="vacuumDemo">}}
<br />

{{<image-responsive src="media/white.jpg" alt="white">}}
{{<image-responsive src="media/vacuumDemo2.jpg" alt="vacuumDemo2">}}
<br />

# Download

[Box_zund_ai](media/box_zund_ai.zip)
