+++
title="Project Management"
lead = "How to create first GitLab repository from scratch"
+++
In this week, I created my first GitLab repository and explored Git, Html, Markdown, Hugo, Bootstrap, ImageMagick and FFmpeg. The text editor I chose is Visual Studio Code because I have used it for quite a long time. 

The first thing I did is to make my terminal fancy. I installed [iTerm2](https://iterm2.com) and [Oh My Zsh](https://github.com/ohmyzsh/ohmyzsh) and powerline font. Here is a screenshot of what it looks now:

(Play around with "ls")

{{<image-responsive src="ls_resize.jpg" alt="ls">}}
<br />

To insert code snippets with nice format, I used syntax highlighting in Hugo. Please find it in the section below.


## Syntax Highlighting in Hugo
Hugo has built-in code highligher - Chroma. 
To use it, I include following line in config.toml:

```go 
pygmentsStyle = "monokai"
```
"monokai" is one of the styles Chroma has. There are other [Chroma Styles](https://xyproto.github.io/splash/docs/all.html).

Check out Hugo tutorial [here](https://gohugo.io/content-management/syntax-highlighting/).

## Git
Here is a good reference material - [Git book](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)

Install Git via Homebrew
```go 
brew install git
```

Open the terminal and go to that directory
```go 
cd /Users/dinan/Documents/AALTO/20p3/fab/demo
```

Initialize a repository
```go 
git init
```

Config name and email
```go 
git config --global user.name "Dinan Yan"
git config --global user.email "dinanyan@example.com"
```



Check current status of repository
```go 
git status
```

Sync the change
```go 
git add .
```

Commit and add message
```go 
git commit -m "Initial version"
```
Show changes
```go 
git diff
```

Check the history
```go 
git log
```

Push to GitLab
```go 
git push
```


## Set up GitLab
Generate SSH 
```go
ssh-keygen -t ed25519 -C "Demo Key"
```

Show SSH
```go
cat id_ed25519.pub
```
Add that SSH key on User Settings page

Create a blank project and copy paste the code snippet on gitlab to initialize

Create README.md
```go 
touch README.md
```

Add gitlab-ci.yml from dropdown on repository page

## Markdown
[Markdown guide](https://about.gitlab.com/handbook/markdown-guide/)
* \# h1 
  # h1

* \## h2
  ## h2

* \###### h6
  ###### h6

* \[GitLab](http://gitlab.com)
  
  [GitLab](http://gitlab.com)


## ImageMagick
[ImageMagick](https://imagemagick.org) is a command-line tool for image conversion and processing

Following are a few useful command:

Display image info
```go 
identify "portrait.jpg"
````
Resize
```go
convert -resize 1280x720 "portrait.jpg" "portrait.jpg_resize.jpg"
convert -resize 1280x720 -quality 65 "portrait.jpg" "portrait_resized_65.jpg"
```
## FFmpeg
[FFmpeg](https://ffmpeg.org) is a collection of libraries and tools to process multimedia content such as audio, video, subtitles and related metadata.

Display video info
```go 
ffmpeg -i "ex.mp4"
```

Resize and cut the video
```go 
ffmpeg -i "ex.mp4" -ss 4 -t 5 -vf scale=1280x720 "ex_resize.mp4"
```

## Hugo
[Hugo](https://gohugo.io) is an open-source static site generator. [Shortcodes](https://gohugo.io/content-management/shortcodes/) in Hugo are quite handy. They are simple snippets inside content files calling built-in or custom templates.
##### Shortcode example \<video>
To use it, first create a folder under layouts called "shortcodes". Create video.html file.
```go 
<video controls muted = "true" autoplay = "true" loop = "true" width="500">
    <source src = "{{ .Inner}}" 
            type = "video/mp4">
    Sorry, your browser doesn't support embeded videos.
</video>
```

Then go to markdown file where you want to insert the video
```go
{{\<video>\}}ex.mp4{{\</video >\}}
```

## Bootstrap
[Bootstrap](https://getbootstrap.com) is used to design and customize responsive mobile-first sites.

Quickstart: copy paste boostrap.min.css file into static/assets/css and boostrap.min.js file into static/assets/js


