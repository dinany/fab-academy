+++
title="Electronics Production"
lead = "Make an in-circuit programmer"
+++

# Individual Assignment

Given the following png files, I made an in-circuit programmer by milling and stuffing the PCB board.

{{<image-responsive src="traces.jpg" alt="Traces">}}
{{<image-responsive src="interior.jpg" alt="Interior">}}

I used [mods](http://mods.cba.mit.edu) to convert png to rml file for milling.
For outline, I clicked "mill outline" to get the default setting and manully entered the tool diamter 0.8, cut depth 0.6, max depth 1.8 and kept the offset number to 1.
{{<image-responsive src="outline_rml.jpg" alt="Interior Rml">}}

For traces, I chose tool diamter 0.4, cut depth 0.1, max depth 0.1. I prepared two rml files for traces, one with offset 3 and the other with 4.

{{<image-responsive src="trace_rml_offset3.jpg" alt="Traces Rml">}}
<br />

I milled on SRM-20 for the rml file with offset 4 and used MDX-40 for offset 3. Here is the comparsion:

{{<image-responsive src="pcb.jpg" alt="PCV">}}
<br />

They have quite big difference. It has wider space with offset 4 but some of the copper traces are too fine to stay in place. I chose offset 3 to continue soldering electronic components.

One key thing for milling with SRM-20 is to adjust the length of the drilling tool. I left it too long outside the fastener and it broke after a few minutes milling because of the vibration. One trick Solomon showed me is that loose the fastener and press the arrow down a bit after the header touches the board.

{{<image-responsive src="srm1.jpg" alt="SRM1">}}
<br />

{{<image-responsive src="srmsetting.jpg" alt="SRM Setting">}}
<br />

MDX-40 could use sensor to set Z origin and this feature is quite handy. But one tricky part when using MDX-40 is to tighten the drilling tool with two wrenches. Once I messed up with the direction and mistakenly loosed the screws so that the drilling tool dropped on the wood and broke the head. I learned that I should tighten a bit by hand first and then use wrenches in order to avoid that accident.

{{<image-responsive src="mdx1.jpg" alt="MDX1">}}
<br />

{{<image-responsive src="mdx2.jpg" alt="MDX2">}}
<br />

Always double check the setup before cutting. I chose RML-1 because I was using mods.

{{<image-responsive src="mdx3.jpg" alt="MDX3">}}

{{<image-responsive src="mdx4.jpg" alt="MDX4">}}
<br />

Following is a list of all the electronic componenets needed. I wrote down all the parts shown in Neil's picture and found them from lab collection. At first, it was painful to look for one single part from all those drawers. I stood there for half an hour searching for "UPDI", which is labeled "2 Position Receptacle Connector" in the collection. But I believe I'll be familiar with their positions after a few projects.

{{<image-responsive src="list.jpg" alt="List">}}

<br />

Then I did soldering. One biggest mistake I made was that I forgot to align the small circle on the FT chip and reversed the direction. I didn't find out until I soldered one side of the chip. Thanks to Solomon, who helped me cleaned the mess and saved my board. One essential thing is to use flux as much as possible everytime before soldering because it would make things a lot easier.

{{<image-responsive src="desoldering.jpg" alt="Desolder">}}

<br />

When I was soldering the chip at the second time, I tried to remove some solder by using wig. But I overheated the chip and broke one leg from it. Even the copper trace underneath went off. So I re-did the whole thing again, starting from milling the PCB board. But I forgot to reset z direction when I changed milling tool on MDX-40 so I broke the 0.8 mm tool. One important lesson I learned from it is that never do things in a rush. For the outline, I can also use the 1.5 mm tool or any model above 0.8mm.

I re-did the soldering under the help of Kris. When we debugged the chip, we found two mistakes I made. First, I soldered wrong USB type as following. So Kris helped remove it and added a black sticker underneath the board to make stable connection.

{{<image-responsive src="usb.jpg" alt="USB">}}
<br />

Second, I used wrong resistors. Instead of 49.9, I soldered 49.9k, which is way too big.

Finally here is my result after breaking so many tools:

{{<image-responsive src="hero.jpg" alt="Heroshot">}}

<br />

I did test on macos and linux system and they both worked well.

{{<image-responsive src="macos.jpg" alt="Mac">}}
<br />

{{<image-responsive src="linux.jpg" alt="Linux">}}
<br />

# Group Assignment

I am in the group with Eero Pitkänen and Onni Eriksson. Please find our group assignment [here](https://eeropic.gitlab.io/fablab-doc/assignments/assignment-05-group/).
