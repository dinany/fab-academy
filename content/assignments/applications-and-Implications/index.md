+++
title="Applications and Implications"
lead = "Propose a final project masterpiece that integrates the range of units covered"
+++

##### Requirements:

- Your project should incorporate 2D and 3D design, additive and subtractive fabrication processes, electronics design and production, embedded microcontroller interfacing and programming, system integration and packaging

- Where possible, you should make rather than buy the parts of your project

- Projects can be separate or joint, but need to show individual mastery of the skills, and be independently operable

# What will it do?

# Who's done what beforehand?

# What will you design?

# What materials and components will be used?

# Where will come from?

# How much will they cost?

# What parts and systems will be made?

# What processes will be used?

# What questions need to be answered?

# How will it be evaluated?
