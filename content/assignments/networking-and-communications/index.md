+++
title="Networking and Communications"
lead = "Serial Communication between boards"
+++

**Assignment:**

- design, build, and connect wired or wireless node(s) with network or bus addresses
- send a message between two projects

What I want to achieve is to send a binary message from computer via serial communication to two magnet boards.

I plan to use the adaptor and two magnet boards from output devices week.

The computer will send four types of message:

- 0000 0000
- 0100 0000
- 1000 0000
- 1100 0000

The first digit specifies which board has to pick up the message.

The second digit is the state: pin on (1) or off (0).

Here is the code:

#### Arduino Code

Secondary ID 0

```go
const int Gate = 2; //  PA1 of the ATtiny412
byte incomingByte;
int id = 0;

void setup() {
  pinMode(Gate, OUTPUT);

  Serial.begin(115200);
}

void loop() {
  while (Serial.available()>0) {
    incomingByte = Serial.read();
    int secondary = (incomingByte & (1 << 7)) >> 7;  //00000001 << 7 => 10000000
    int state = (incomingByte & (1 << 6 )) >> 6; //00000001 << 6 => 01000000
    if(secondary != id){
      continue;
    }

    if (state>0) {
      digitalWrite(Gate, HIGH);
    }else {
      digitalWrite(Gate, LOW);
    }
  }

  delay(100);
}
```

Secondary ID 1

```go
const int Gate = 2; //  PA1 of the ATtiny412
byte incomingByte;
int id = 1;

void setup() {
  pinMode(Gate, OUTPUT);

  Serial.begin(115200);
}

void loop() {
  while (Serial.available()>0) {
    incomingByte = Serial.read();
    int secondary = (incomingByte & (1 << 7)) >> 7;  //00000001 << 7 => 10000000
    int state = (incomingByte & (1 << 6 )) >> 6; //00000001 << 6 => 01000000
    if(secondary != id){
      continue;
    }

    if (state>0) {
      digitalWrite(Gate, HIGH);
    }else {
      digitalWrite(Gate, LOW);
    }
  }

  delay(100);
}
```

#### Processing Code

```go
import processing.serial.*;
Serial port;

int recLength=50;
PFont font;

void setup()
{
  size(800,600);
  println(Serial.list());
  port = new Serial(this, "/dev/tty.usbserial-FTBVM4SY", 115200);

  font=loadFont("AppleGothic-48.vlw");
  textFont(font);
}

void draw()
{
  background(255);


  textSize(32);
  fill(0);
  stroke(255);
  text("Hover over squares to turn on the magnets",100,50 );

  textSize(20);
  text("MAGNET 0", width/2-170, height/2);
  text("MAGNET 1", width/2+80, height/2);


  fill(200);
  rect(width/2-150,height/3,recLength,recLength);
  fill(200);
  rect(width/2+100,height/3,recLength,recLength);

  if (inside(width/2-150, height/3, width/2-150+recLength, height/3+recLength, mouseX, mouseY))
  {
    fill(0);
    rect(width/2-150,height/3,recLength,recLength);
    fill(200);
    rect(width/2+100,height/3,recLength,recLength);
    port.write(64); //0100 0000
  }

  else if (inside(width/2+100, height/3, width/2+100+recLength, height/3+recLength, mouseX, mouseY))
  {
    fill(0);
    rect(width/2+100,height/3,recLength,recLength);
    fill(200);
    rect(width/2-150,height/3,recLength,recLength);
    port.write(192); //1100 0000


  }

  else
  {
    port.write(0); //0000 0000
    port.write(128);//1000 0000
  }


}

//funtion taking top left and bottom right corner coordinates for a rectangle
//returns 1 if mouse on that rectangle, otherwise 0.
boolean inside(int left, int top, int right, int bottom, int x, int y)
{
  //check whether the mouse is inside the square, four edges
  if (x>left && x<right && y>top && y<bottom)
    return true;
  else
    return false;
}
```

# Processing interface

{{<image-responsive src="media/interface.jpg" alt="Interface">}}
<br />

# Demo

{{<video>}}media/demo.mp4{{</video>}}
