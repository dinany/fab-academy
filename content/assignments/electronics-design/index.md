+++
title="Electronics Design"
lead = "Make an echo hello-world board"
+++

**Group**:

- use the test equipment in your lab to observe the operation of a microcontroller circuit board

**Individual**:

- redraw an echo hello-world board,
- add (at least) a button and LED (with current-limiting resistor)
- check the design rules, make it, and test it
- extra credit: simulate its operation

# Individual Assignment

I used KiCAD to draw the board, created rml file in mods and milled with MDX-40. Here is the final outcome:

{{<image-responsive src="media/helloboard.jpg" alt="Hello Board">}}
<br />

For the integrated circuit, I chose ATtiny 412. Below are the components I used.

{{<image-responsive src="media/list_resize.jpg" alt="Components List">}}
<br />

### KiCAD

I created schematic design as below.

{{<image-responsive src="media/eeschema_result.jpg" alt="Eeschema Result">}}
<br />

One tricky part is to figure out how to connect RX and TX. TX and RX are abbreviations for Transmit and Receive. Here is the pinout demonstration of [ATtiny 412](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.md). It took me a while to understand what it stands for. Instead of saying PA6 has to connect to TX, it means PA6 is the location of TX on the chip. In order to make the chip work, PA6 (TX) has to connect to RX and PA7 (RX) has to connect to TX.

{{<image-responsive src="media/ATtiny_x12.gif" alt="Pinout">}}
<br />

After finishing the schematic drawing in eeschema, I generated netlist and opened in pcbnew by pressing "Update PCB".

{{<image-responsive src="media/pcbview1.jpg" alt="Pcbview1">}}
<br />

I met a problem in this step. The interface was blank after I imported the netlist file.

{{<image-responsive src="media/pcbview2.jpg" alt="Pcbview2">}}
<br />

Finally I figured out that it was caused by default mode "Modern Toolset (Fallback)". If I changed it to "Legacy Toolset" or "Modern Toolset (Accelerated)", the graphics showed up. However, it seems that the "Legacy Toolset" is no longer maintained or supported. Some functions do not work under that mode. For instance, "Add graphic polygon" can't be used. So I went with "Modern Toolset (Accelerated)" in the end.

{{<image-responsive src="media/pcbview3.jpg" alt="Pcbview2">}}
<br />

Before routing the copper wires, I set up the track width by clicking "Track" and then "Edit pre-defined sizes...". Under "Net Classes", I specified "Clearance" and "Track Width" to be 0.4mm, which is the average tool size to mill the PCB. I connected the communication lines and positive voltages and left ground open.

{{<image-responsive src="media/design_rules.jpg" alt="Design Rules">}}
<br />

To auto fill the ground connections, I selected front copper layer and chose "/GND". I specified "Clearance", "Minimum width", "Thermal clearance", and "Thermal spoke width" as 0.8, 0.4, 0.5, 0.5. Thermal clearance has to be bigger than the minimum width.

{{<image-responsive src="media/fill.jpg" alt="Fill">}}
<br />

At this stage, I checked all the connections by pressing the bug icon.

{{<image-responsive src="media/gnd.jpg" alt="Ground Auto Connect">}}
<br />

**Margin**

I added margin around the board, otherwise mods wouldn't be able to create path around it. Just to make sure the gap between margin and actual board is bigger than the tool size, I switched the grid to 1mm and started with 2mm margin. I selected margin layer and drew a rectangle or any other shape. When it is exported, the margin is always rectangle anyway.

**Filled**

For the outline, mods doesn't work well with empty polygon but filled one. I selected drawing layer - Dwgs.User and activate "Add graphic polygon". Since I designed rounded corner, I drew several short lines linked together to mimic arc. Then I selected the polygon, pressed "e" and edited the line thickness to 0.

{{<image-responsive src="media/thickness.jpg" alt="Thickness">}}
<br />

The final result in pcbnew looks like this:

{{<image-responsive src="media/pcbnew_result.jpg" alt="Pcbnew Result">}}
<br />

When I exported it, I chose the front copper layer and Dwgs.User layer.

### Milling

I used [mods](http://mods.cba.mit.edu) to create milling path file.

For the outline, I specified tool size to be 1.5mm with speed 1.5mm/s.

{{<image-responsive src="media/cutting-1mm5-1mms5.jpg" alt="Outline mods">}}
<br />

For the traces, I specified tool size to be 0.4mm with speed 20mm/s for the new bits and 1.5mms/s for the old bits in lab.

{{<image-responsive src="media/traces-0mm4-20mms.jpg" alt="Traces mods">}}
<br />

However, I clicked twice "invert" so the picture was actually flipped back.

{{<image-responsive src="media/notInverted.jpg" alt="Not inverted mods">}}
<br />

Here is the outcome of mistakenly inverted back board.

{{<image-responsive src="media/compare.jpg" alt="Not inverted comparison">}}
<br />

The following are the bits I used - 0.4mm/1.5mm/0.2-0.5mm(new bits).

{{<image-responsive src="media/bit0mm4.jpg" alt="Bit 0.4mm">}}
<br />
{{<image-responsive src="media/bit1mm5.jpg" alt="Bit 1.5mm">}}
<br />
{{<image-responsive src="media/newbit.jpg" alt="New Bit">}}
<br />

The new bit in our lab can mill with high speed. But the outcome was quite rough. I still don't know if it is caused by the speed.

The following are videos to compare milling speed between 0.4mm flat bit and 0.2-0.5mm new bit.

0.4mm flat bit:

{{<video>}}media/oldbit.mp4{{</video >}}

0.2-0.5mm new bit:

{{<video>}}media/newbit.mp4{{</video >}}

The first time I used fast bit in MDX-40, it didn't go deep enough to mill the traces. But when I manually lowered the z-axis and re-milled again on the same board, it cut too deep as you can see in the picture below. The left is milled by old 0.4mm bit with 1.5mm/s speed. The right is milled by new bit with 20mm/s speed.

{{<image-responsive src="media/compare2.jpg" alt="New Bit comparison">}}
<br />

I found soldering not as hard as last time since I applied a lot of flux beforehand.

{{<image-responsive src="media/flux.jpg" alt="Flux">}}
<br />

I used Arduino to test the board. Please find the test code on [Kris's gitlab page](https://gitlab.com/kriwkrow/hello-t412).

{{<image-responsive src="media/code.jpg" alt="Test Code">}}
<br />

To run the code, I first installed [megaTinyCore board library](https://github.com/SpenceKonde/megaTinyCore/blob/master/Installation.md).

{{<image-responsive src="media/install.jpg" alt="Install">}}
<br />

Then I selected "ATtiny412" and "Serial Port and 4.7k (pyupdi style)" on Arduino interface.
{{<image-responsive src="media/port.jpg" alt="Select Port">}}
<br />

Uploaded!

{{<video>}}media/helloboard640.mp4{{</video >}}

# Group Assignment

I am in the group with Eero Pitkänen, Onni Eriksson and Ruo-xuan Wu. Please find our group assignment [here](https://russian_wu.gitlab.io/fab-academy-2021/assignments/week07-electronics-design/week07-electronics-design/).

# Download

[hello-t412-F_Cu.svg/hello-t412-Dwgs_User.svg](media/HelloBoard_kicad_svg.zip)

[hello-t412-kicad](media/hello-t412.zip)
