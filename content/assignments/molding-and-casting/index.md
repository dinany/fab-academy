+++
title="Molding and Casting"
lead = "Make an ice mold for shot"
+++

**Assignment**:

- review the safety data sheets for each of your molding and casting materials, then make and compare test casts with each of them
- design a mold around the stock and tooling that you'll be using, mill it (rough cut + (at least) three-axis finish cut), and use it to cast parts

I want to make an ice mold for shot. First I designed a 3D model in Fusion.

## Fusion 360

{{<fusion src="https://aalto180.autodesk360.com/g/shares/SH56a43QTfd62c1cd9680976329a7960c15e">}}

## VCarve

I used VCarve to create tool path for milling.

- First, enter the stock size and choose Z zero position to be MATERIAL SURFACE.
- Then Select the zero plane position in model to be the bottom of the slider.
- Align the model to the center of the stock.
- Define the gap above model to smooth the surface. But in my case, since the model height is close to the stock height, I actually entered zero.

{{<image-responsive src="media/Capture.JPG" alt="Capture1">}}
{{<image-responsive src="media/Capture2.JPG" alt="Capture2">}}
{{<image-responsive src="media/Capture3.JPG" alt="Capture3">}}
{{<image-responsive src="media/Capture4.JPG" alt="Capture4">}}
{{<image-responsive src="media/Capture5.JPG" alt="Capture5">}}
<br />

- Select roughing/finishing tool

{{<image-responsive src="media/Capture6.JPG" alt="Capture6">}}
{{<image-responsive src="media/Capture7.JPG" alt="Capture7">}}
<br />

I selected Endmill 3mm for both roughing and finishing. Here is the preview below. It looks terrible.

{{<image-responsive src="media/Capture8.JPG" alt="Capture8">}}
<br />

Then I selected Endmill 3mm for roughing and Ball Nose 3mm for finishing. The result looks much better but quite similar as not using finishing tool at all.

{{<image-responsive src="media/Capture9.JPG" alt="Capture9">}}
<br />

Solomon suggested to just use roughing and skip finishing step because of the simple structure of my model. He also introduced how to create everything from scratch in VCarve so that I don't have to import my Fusion file.

{{<image-responsive src="media/Capture10.JPG" alt="Capture10">}}
<br />

## MDX-40

I used MDX-40 for milling.

Setup

{{<image-responsive src="media/Capture11.JPG" alt="Capture11">}}mdx
{{<image-responsive src="media/Capture12.JPG" alt="Capture12">}}
<br />

{{<image-responsive src="media/mdx.jpg" alt="MDX">}}
<br />

I made a mistake and used short bit at first.

{{<image-responsive src="media/bit38.jpg" alt="bit38">}}
<br />

The collet hit the boundaries when the depth reached tool length. Because of the force, the wax moved and I had to stop the machine and redo it.

{{<image-responsive src="media/mistake.jpg" alt="Mistake">}}
<br />

I chose the right length as below.

{{<image-responsive src="media/bit75.jpg" alt="bit75">}}
<br />

And here is the result.

{{<image-responsive src="media/wax.jpg" alt="wax">}}
<br />

## Sorta Clear 37

I used [Sorta Clear 37](https://www.smooth-on.com/products/sorta-clear-37/) because it is food safe. The pot life is 25 mins and curing time is 4 hrs. The mix ratio is 1A:1B by volume.

{{<image-responsive src="media/datasheet.jpg" alt="Datasheet">}}
{{<image-responsive src="media/bottle.jpg" alt="Bottle">}}
<br />

Here is the workstaion. Always wear gloves when working there.

{{<image-responsive src="media/workstation.jpg" alt="Workstaion">}}
<br />

To know how much silicone rubber I need, I filled the mold with water and measure the weight.

{{<image-responsive src="media/water.jpg" alt="Water">}}
<br />

I need 65g in total. I put down 100g because there would be some waste on the wood stick or cup edges.

{{<image-responsive src="media/measure.jpg" alt="Measure">}}
{{<image-responsive src="media/note.jpg" alt="Note">}}
<br />

To kill the bubbles, I placed the wax with sillicone inside the dome and started the vacuum pump.

{{<image-responsive src="media/vacuum.jpg" alt="Vacuum">}}
<br />

However, no matter how many times I tried, there were still a lot of bubbles.

{{<image-responsive src="media/bubbleInWax.jpg" alt="Bubble in Wax">}}
<br />

The most challenging part is to remove the mold from the wax after it fully cured. It took me around 30 mins to fight with them. Finally I broke the wax..because the bottom is too thin. But thanks to that, I could push the mold out from the bottom.

{{<image-responsive src="media/broke.jpg" alt="Broke">}}
<br />
{{<image-responsive src="media/bubble.jpg" alt="bubble">}}
<br />

BIG MISTAKE: The mold doesn't leave a base space for the ice. I checked my Fusion model and it looks good. It resulted from VCarve process because I didn't import 3d model from fusion. And I forgot to take into account the base when we redrew the model in VCarve.

QUICK SOLUTION: Use knife to cut out a small piece.

{{<image-responsive src="media/cut.jpg" alt="cut">}}
<br />

Heroshot:

{{<image-responsive src="media/heroshot.jpg" alt="heroshot">}}
<br />

## Download

Please find fusion file at the top of the page.
