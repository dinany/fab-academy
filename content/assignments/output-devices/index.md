+++
title="Output Devices"
lead = "Electromagnets"
+++

**Assignment**:

- add an output device to a microcontroller board you've designed, and program it to do something
- measure the power consumption of an output device

I plan to work with electromagnet in this week to prepare for my final project.

Kris ordered a few [5V Electromagnet - 2.5 Kg Holding Force - P20/15](https://www.adafruit.com/product/3872#description).

In the description on Adafruit website, it says that "since its a coil, you'll need to use a motor or solenoid driver with kick-back protection."

That specific electromagnet requires 220mA at 5V. I checked the Electrical Characteristics in Attiny412 datasheet. Under 35.2 Absolute Maximum Ratings, the max current of each I/O pin is 40mA, which is much smaller than 220mA. That's why we need to bring in the motor driver, to be specific, the H-bridge.

[How Transistors Work](https://www.build-electronic-circuits.com/how-transistors-work/)

[What Is an H-Bridge](https://www.build-electronic-circuits.com/h-bridge/)

I found [Allegro A4953](https://www.digikey.com/product-detail/en/allegro-microsystems-llc/A4953ELJTR-T/620-1428-1-ND) in fablab.

{{<image-responsive src="media/a4953-1.jpg" alt="a4953-1">}}
{{<image-responsive src="media/a4953-2.jpg" alt="a4953-2">}}
<br />

{{<image-responsive src="media/a4953-3.jpg" alt="a4953-3">}}
<br />

Kris suggested that I should use MOSFET and regulator instead of H-bridge because H-bridge is for controlling motors. I don't need to control current direction.

Illustration by Kris:

{{<image-responsive src="media/kris1.jpg" alt="kris1">}}
<br />

I decided to add a 2x2 headers for networking assignment next week.

{{<image-responsive src="media/kris2.jpg" alt="kris2">}}
<br />

### How does MOSFET work?

The MOSFET transistor has three pins: Gate, Source, and Drain.

MOSFET works similar to the NPN transistor

- In the NPN transistor, the **current** from **base** to **emitter** decides how much current can flow from collector to emitter.
- In the MOSFET transistor, the **voltage** between **gate** and **source** decides how much current can flow from drain to source.

#### To Turn on

A amount of voltage bigger than threshold have to apply between Gate and Source. The datasheet of [N-Channel MOSFET (1.7A, 30V)](media/NDS355AN-D.PDF) indicates the Gate Threshold Voltage is 1.6v.

#### To Turn off

When you apply a voltage between gate and source, this voltage stays there until it’s discharged.

Therefore, to turn off the MOSFET, I added a resistor as below. With the resistor, there is a path for the gate-source capacitor to discharge so that the transistor turns off again.

{{<image-responsive src="media/dn.jpg" alt="dn">}}
<br />

#### Regulator

According to the datasheet of [NCP1117](media/NCP1117-D.PDF), I added two 10uF capacitors.

{{<image-responsive src="media/regulator.jpg" alt="regulator">}}
<br />

### Electronics components

1. ATtiny412
2. 10uF capacitors x 3
3. UPDI 1x2 header
4. 2x2 header
5. 1x2 Screw Terminal
6. 10K resistors x 2
7. MOSFET N-CH
8. LED x 2
9. Regulator
10. Power Jack

Here's the schematic part:

{{<image-responsive src="media/sch_magnet.jpg" alt="sch_magnet">}}
<br />

{{<image-responsive src="media/pcb_magnet.jpg" alt="pcb_magnet">}}
<br />

### CopperCam

I used CopperCam to generate tool path since it's easier for creating drilling path than mods.

One issue that trapped me was that the front copper layer disappeared after I imported drilling file.

{{<image-responsive src="media/Capture.JPG" alt="Capture">}}
<br />

Solution is to redefine the dimension!

{{<image-responsive src="media/Capture2.JPG" alt="Capture2">}}
<br />

Align the drilling spots by using reference pads.

{{<image-responsive src="media/Capture4.JPG" alt="Capture4">}}
<br />

Define the hole size.

{{<image-responsive src="media/Capture5.JPG" alt="Capture5">}}
<br />

Set origin point.

{{<image-responsive src="media/Capture6.JPG" alt="Capture6">}}
<br />

Choose tools.

{{<image-responsive src="media/Capture7.JPG" alt="Capture7">}}
<br />

Got the board! Nice and clean!

{{<image-responsive src="media/magnet_board.jpg" alt="Magnet board">}}
<br />

To test and program, I made a adaptor board.

{{<image-responsive src="media/sch_adaptor.jpg" alt="sch_adaptor">}}
<br />

{{<image-responsive src="media/pcb_adaptor.jpg" alt="pcb_adaptor">}}
<br />

Connect them together:

{{<image-responsive src="thumbnail.jpg" alt="Heroshot">}}
<br />

### Arduino

```go
const int Gate = 2; //  PA1 of the ATtiny412
int incomingByte;      // a variable to read incoming serial data into

void setup() {

  // initialize serial communication:

  Serial.begin(115200);

  // initialize the LED pin as an output:

  pinMode(Gate, OUTPUT);
}

void loop() {
  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    if (incomingByte == 'H') {
      digitalWrite(Gate, HIGH);
    }
    if (incomingByte == 'L') {
      digitalWrite(Gate, LOW);
    }
  }
}
```

Demo video:

{{<video>}}media/demo.mp4{{</video >}}
<br />

### Useful Reference

- [Physical Pixel](https://www.arduino.cc/en/Tutorial/BuiltInExamples/PhysicalPixel)
- [Daniel Rosenberg](http://fab.cba.mit.edu/classes/863.11/people/daniel.rosenberg/)

### Download

- kicad [magnet board](media/magnet.zip)
- kicad [adaptor board](media/adaptor.zip)
