+++
title="3D Scanning and Printing"
lead = "Try out 3D scanner and printer"
+++

# Individual Assignment

## 3D Scanning

The 3D scanner I tried for this week is Asus Xtion, which is a handheld infrared sensor and can be used together with [Skanect Pro](https://skanect.occipital.com) software.

I scanned the following object I found on the table in fablab.

{{<image-responsive src="media/3d-scanning/E.jpg" alt="E">}}
<br />

In Skanect user interface, I chose "object".

{{<image-responsive src="media/3d-scanning/skanect1.jpg" alt="Skanect1">}}

{{<image-responsive src="media/3d-scanning/skanect2.jpg" alt="Skanect2">}}
<br />

I forgot to scan the top of the object and here is the result after I cleaned up all the small particles around the object and filled up holes.

{{<image-responsive src="media/3d-scanning/Eview1.jpg" alt="View1">}}

{{<image-responsive src="media/3d-scanning/Eview2.jpg" alt="View2">}}
<br />

I did another scan on the following object, which I made in the metal workshop.

{{<image-responsive src="media/3d-scanning/steel.jpg" alt="Steel">}}
<br />

It failed to capture the arms. I guess the reason is that the arms are too thin and also the steel refracting lights is tricky to capture. I suppose the lighting condition would also affect the scanning quality. When sun light came through the window and shined on the object, it was diffcult to capture and there was always alert saying moving too fast or go back to previous position even though I stopped spinning the object.

{{<image-responsive src="media/3d-scanning/skanect3.jpg" alt="Skanect3">}}
<br />

{{<image-responsive src="media/3d-scanning/steel_view.jpg" alt="Steel view">}}
<br />

## 3D Printing

This week, I experimented LulzBot Mini with PLA material. Each LulzBot is connected to a Raspberry Pi. It is really convenient because it doesn't need SD card or USB to transfer gcode files. I can simply upload gcode and control the printer via web server.

I printed a ring and keychains, which I designed with Fusion 360 and documented in [Computer-Aided Design](https://dinany.gitlab.io/fab-academy//assignments/computer-aided-design/) page.

I imported stl file to Cura LulzBot. I set the layer height to be 0.1mm and speed to be 60mm/s for smoothier surface and more detailed outcome.

{{<image-responsive src="media/3d-printing/ring1.jpg" alt="Cura Ring">}}
<br />

I engraved the text "Don't forget to reset Z axis" to remind myself when dealing with the milling machine. I thought the text would be rought since those letters are small but the outcome is quite detailed.

{{<image-responsive src="media/3d-printing/ring_result.jpg" alt="Ring">}}
<br />

For the keychains, I printed two versions with different parameters.

One with layer height 0.25 mm and speed 60 mm/s.

{{<image-responsive src="media/3d-printing/keychain1.jpg" alt="Cura keychain1">}}
<br />

The other with layer height 0.15 mm and speed 70 mm/s. I increased the speed because otherwise it would take too long to finish.

{{<image-responsive src="media/3d-printing/keychain2.jpg" alt="Cura keychain2">}}
<br />

I should have wiped the printing area with isopropyl alcohol but I forgot. So it was difficult to remove the object after it was ready with bare hands. I used a knife to help in the end.

{{<image-responsive src="media/3d-printing/process.jpg" alt="Process">}}
<br />

With layer height 0.15 mm, there are smaller gaps or holes on the letters.

{{<image-responsive src="media/3d-printing/keychain_result.jpg" alt="Keychain">}}
<br />

# Group Assignment

I am in the group with Eero Pitkänen and Onni Eriksson. Please find our group assignment [here](https://eeropic.gitlab.io/fablab-doc/assignments/assignment-06-group/).

# Download

[gcode files](media/gcode.zip) for 3D printing

Please find CAD files in [Computer-Aided Design](https://dinany.gitlab.io/fab-academy/assignments/computer-aided-design/) page.
