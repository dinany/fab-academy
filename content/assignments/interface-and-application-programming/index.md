+++
title="Interface and Application Programming"
lead = "Make an interface for magnet board"
+++

**Individual**:

- write an application that interfaces a user with an input &/or output device that you made

**Group**:

- compare as many tool options as possible

For the visuals, I used the code in the Alive Dead Media course.

A few screenshots below:

{{<image-responsive src="media/interface1.jpg" alt="Interface1">}}
<br />

{{<image-responsive src="media/interface2.jpg" alt="Interface2">}}
<br />

{{<image-responsive src="media/interface3.jpg" alt="Interface3">}}
<br />

### Processing Code

```go
import processing.serial.*;
Serial port;

int recLength=50;
PFont font;

PImage p1,p2,p3,grad;

int distance[]=new int[800*600],
    angle[]=new int[800*600];

void setup()
{
  size(800,600);
  println(Serial.list());
  port = new Serial(this, "/dev/tty.usbserial-FTBVM4SY", 115200);

  font=loadFont("AppleGothic-48.vlw");
  textFont(font);

  p1=loadImage("rusty.jpg");
  p2=loadImage("blue.jpg");
  p3=loadImage("dust.jpg");
  grad = loadImage("gradient.png");

  p1.loadPixels(); // Make pixels available
  p2.loadPixels();
  p3.loadPixels();

  //Calculate angle and distance for each pixel
  //i.e. polar coord
  for(int y=0; y<height; y++)
  {
    for(int x=0;x<width;x++)
    {
      distance[y*width+x]=int(dist(x,y,width/2,height/2));
      distance[y*width+x]=30000/(50+distance[y*width+x]);

      // -PI..PI => 0...2PI => 0..999 SINCE stonewall.png is 1000x1000
      angle[y*width+x]=int((atan2(y-height/2,x-width/2)+PI)*999/(PI*2));
    }
  }
}

void draw()
{
  background(255);

  tunnel(p3);

  textSize(32);
  fill(255);
  stroke(255);
  text("Hover over squares to turn on the magnets",100,50 );

  textSize(20);
  text("MAGNET 0", width/2-170, height/2);
  text("MAGNET 1", width/2+80, height/2);


  fill(200);
  rect(width/2-150,height/3,recLength,recLength);
  fill(200);
  rect(width/2+100,height/3,recLength,recLength);

  if (inside(width/2-150, height/3, width/2-150+recLength, height/3+recLength, mouseX, mouseY))
  {
    tunnel(p1);
    fill(0); //white
    rect(width/2-150,height/3,recLength,recLength);
    fill(200);
    rect(width/2+100,height/3,recLength,recLength);
    port.write(64); //0100 0000
  }

  else if (inside(width/2+100, height/3, width/2+100+recLength, height/3+recLength, mouseX, mouseY))
  {
    tunnel(p2);
    fill(0); //white
    rect(width/2+100,height/3,recLength,recLength);
    fill(200);
    rect(width/2-150,height/3,recLength,recLength);
    port.write(192); //1100 0000


  }

  else
  {
    port.write(0); //0000 0000
    port.write(128);//1000 0000
  }


}

void tunnel(PImage p)
{
  loadPixels();

  //Draw each pixel of the screen
  for(int i=0; i<width*height;i++)
  {
    //add 0xff00000 to rgb value to make it visible. otherwise transparent
    //pixels[i]=0xff000000+distance[i]/2+angle[i];
     pixels[i]=p.pixels[(distance[i]+frameCount)%1000*1000+angle[i]];
  }
  updatePixels();

  blend(grad,0,0,width,height,0,0,width,height,MULTIPLY);
}

//funtion taking top left and bottom right corner coordinates for a rectangle
//returns 1 if mouse on that rectangle, otherwise 0.
boolean inside(int left, int top, int right, int bottom, int x, int y)
{
  //check whether the mouse is inside the square, four edges
  if (x>left && x<right && y>top && y<bottom)
    return true;
  else
    return false;
}

```

### Arduino Code

Please find the arduino code in [Networking page](https://dinany.gitlab.io/fab-academy/assignments/networking-and-communications/).

# Demo

{{<video>}}media/demo.mp4{{</video>}}
