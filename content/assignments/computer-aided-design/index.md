+++
title="Computer-Aided Design"
lead = "Explore FreeCAD"
+++

# Illustrator

I used illustrator a bit before but only knew very basic stuff. I explored some text effect within this week.

One of them is customized text distortion with shape I chose.
{{<image-responsive src="done_resize.jpg" alt="Done">}}

Another is stacked pattern.
{{<image-responsive src="done2_resize.jpg" alt="Done2">}}

### Distortion

To get the first effect above, I add text and draw one circle
{{<image-responsive src="ready_resize.jpg" alt="Get Ready">}}

Bring the circle forward
{{<image-responsive src="forward_resize.jpg" alt="Bring Forward">}}

Align the text to the center of the circle
{{<image-responsive src="align_resize.jpg" alt="Align">}}

Object - Envelope Distort - Make with Top Object
{{<image-responsive src="distort_resize.jpg" alt="Distort">}}

Object - Expand.. Then I got the customized text effect.

### Stacked Pattern

What I did is to add one rectangle on top of the text and split it to grids. Then use the Divide function in Pathfinders to combine them in one layer.
{{<image-responsive src="pathfinder_resize.jpg" alt="Pathfinder">}}

The last thing is to get rid of those paths by using the Exclude function in Pathfinders.
{{<image-responsive src="exclude_resize.jpg" alt="Exclude">}}

# FreeCAD

To start, we can choose "Part Design" from the work bench dropdown on the menu bar.
{{<image-responsive src="part_design_resize.jpg" alt="Start Image">}}
<br />

We can press SPACE to show the hidden elements.
{{<image-responsive src="space_resize.jpg" alt="Space Bar">}}
<br />

To practice, we will make a bottom case for Teensy Audio Board.
{{<image-responsive src="teensy_audio_resize.jpg" alt="Teensy Audio Board">}}
<br />

Step 1 We create a shapebinder and select the bottom face.
{{<image-responsive src="shapebinder_resize.jpg" alt="Shapebinder">}}
<br />

Step 2 We use the reference tool to select the upper circle.
{{<image-responsive src="create_reference_resize.jpg" alt="Reference">}}
<br />

Step 3 We can use this to constrain the radius of the circle but we will do it after we set up the parameter in spreadsheet.
{{<image-responsive src="constrain_radius_resize.jpg" alt="Constrain Radius">}}
<br />

Step 4 We create a new spreadsheet.
{{<image-responsive src="spreadsheet_resize.jpg" alt="Spreadsheet">}}
<br />

Step 5 Enter the parameter and set alias for reference.
{{<image-responsive src="alias_resize.jpg" alt="Alias">}}
<br />

Step 6 Create a pad of of the circle we draw.
{{<image-responsive src="create_pad_resize.jpg" alt="Create Pad">}}
<br />

Step 7 Substitute radius and pad height with alias in spreadsheet.
{{<image-responsive src="set_padradius_resize.jpg" alt="Set Radius">}}
<br />

{{<image-responsive src="set_padheight_resize.jpg" alt="Set Height">}}
<br />

Step 8 We unhide the Teensy and use the same tool above to get the reference of the hole.
{{<image-responsive src="another_reference_resize.jpg" alt="Another Reference">}}
<br />

Step 9 Use the equal method to constrain the circle we draw so that it would be the same size as the reference.
{{<image-responsive src="equal_resize.jpg" alt="Equal">}}
<br />

Step 10 Create a pocket.
{{<image-responsive src="pocket_resize.jpg" alt="Pocket">}}
<br />

Step 11 Set the type to be Through All so that it wouldn't be affected by changes of parameter.
{{<image-responsive src="through_resize.jpg" alt="Through All">}}
<br />

Step 12 Now we finnished Pad 1.
{{<image-responsive src="pad1_resize.jpg" alt="Pad 1">}}
<br />

Step 13 Repeat the process and we get another 2.
{{<image-responsive src="pad3_resize.jpg" alt="Pad 3">}}
<br />

# Fusion 360

I had some experience with Fusion to design boxes for enclosure of electronics parts.

This week, I explored how to create texts on objects using Fusion. I designed a keychain and a ring. Both have text extruded.

I inserted text by pressing "s" and search for "text" option. All the fonts on my laptop are available in Fusion.

{{<image-responsive src="fusion/explode.jpg" alt="Explode">}}
<br />

To turn the text into path, I clicked "Explode Text". Then I can add geometry to the text. Here is the outcome:

{{<image-responsive src="fusion/extrude.jpg" alt="Text extrude">}}
<br />

I also experimented sheet metal. It is quite handy to make text or patterns curved around a cylinder.

{{<image-responsive src="fusion/ring.jpg" alt="Ring">}}
<br />

# Download

Download files: [shred.svg/switch.svg](illustrator.zip) [FreeCAD tryout](TeensyAudioBoard.FCStd) [keychain.stl/ring.stl](fusion/fusion.zip)
