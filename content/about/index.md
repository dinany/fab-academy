+++
title="About Page"
description = "This about page"
+++

### About me


{{<image src="portrait-resized-65.jpg" alt="Portrait Image">}}

<h5>Who am I:</h5>
<p>I am a first-year student in the master's program of New Media Design and Production at Aalto Univeristy. I took the Electronics for Artists course last semester which equipped me some experience with Arduino programming, 3D printing, laser cutting and CAD.</p>
<br />
<br />
<h5>Learning objective:</h5>
<p>My learning objective is to have more hands-on experience with all those machines available in Fablab such as laser cutter and 3D printer. I also would like to learn more about prototype development process for my master thesis. Moreover, I hope to improve my CAD skill throughout the course. Last but not least, I expect to check out and be familiar with electronic parts such as different sensors.</p>
{{</image>}}