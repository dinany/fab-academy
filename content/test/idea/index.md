+++
title="Final Project test "
lead = "My journey to the final project"
+++

## Final project name

## Idea 1:

I would like to make a ferrofluid display that can be interacted with fingers. For example, ferrofluid would move to and stay at the position where our finger points. I really like the motion and visual created by ferrofluid. They are organic, unpredictable and dynamic. So I hope to explore the possibilites of ferrofluid in digital fabrication in the final project. I plan to make an interactive artwork for anyone who is interested to play. I need to do a bit more research in order to finalize my idea.

Inspiration:

{{< youtube dOCGQAXV9FY >}}
<br>

## Idea 2:

I would like to build a snowboard with LED embeded. LED strips could be controlled by accelerometer on the snowboard. Different movements could trigger certain visual effects. The user of this snowboard would be myself and I plan to tailer the flex, shape and length most fit my riding style.

{{<image-responsive src="snowboard_sketch_resize.jpg" alt="Snowboard Sketch">}}
<br />

# Ferrofluid

I decided to go with the first idea.

In the input devices week, I experimented with the [step response](https://dinany.gitlab.io/fab-academy/assignments/input-devices/).

In the output devices week, I played around with [magnets](https://dinany.gitlab.io/fab-academy/assignments/input-devices/).

In what environment can ferrofluid move smoothly? Karl helped me figure out the components of Matti's sample. The transparent liquid around the ferrofluid is composed of water, salt, and saccharose. Its high conductivity comes from salt. Most likely the sugar is added for the density. And the density is 1.1g/ml.

So I mixed water and salt in the ratio 10:1 for the first try.

Here is what I got:

{{<video2>}}media/firstTry.mp4{{</video2>}}

There were some ferrofluid "dead" floating on the surface. It might because the salt didn't fully dissolve in the water and trapped those ferros.

Also the material of the container matters. In this case, I used the plastic cups from Jungle Juice (Collagen Builder is the best! Also love Limely Lemon!). Part of the ferrofluid was stick and stayed on the wall when moving. I'll try glass next time.

To design a multitouch panel, I learned from [Matt Blackshaw](http://fab.cba.mit.edu/classes/863.10/people/matt.blackshaw/week8.html). Schematic design for 18 resolutions multitouch step-response board:

{{<image-responsive src="media/sch.jpg" alt="schematic">}}
<br />

##### Components:

- ATtiny1614
- UPDI
- 2X2 header x4
- Capacitor 1uF
- LED
- R 1K
- R 1M x12
- FTDI

I milled and stuffed the board.

{{<image-responsive src="media/touchBoard18Res.jpg" alt="touchBoard18Res">}}
<br />

I drew the touchpad in Illustrator and used vinyl cutter to cut out the copper sticker.

{{<image-responsive src="media/ai.jpg" alt="ai">}}
<br />

But the outcome was quite rough at the beginning because the blade was almost done.

{{<image-responsive src="media/waste.jpg" alt="waste">}}
<br />

Here is what I got for the first version.

{{<image-responsive src="media/roughcut.jpg" alt="roughcut">}}
<br />

Ideally, three pads connected to the same pin should give different range of values when I touch so that finger can be located precisely. However, they provided similar values. I assume it was caused by noises.

{{<video>}}media/1stDemo.mp4{{</video>}}
<br />

Later, Kris replaced the blade and suggested to replace the cardboard with epoxy layer to cancel some noises.

{{<image-responsive src="media/sandwich.jpg" alt="sandwich">}}
{{<image-responsive src="media/finecut.jpg" alt="finecut">}}
<br />

Setting for the vinyl cutter:

- force: 80gf
- speed: 2cm/s

But still three pads connected to the same pin gave similar values. Plan B is to make each pin connect to single pad and make 3x3 grids.

18-resolution board can still be used as a test board to send message to the magnet board.

{{<image-responsive src="media/testTouchConnectMag.jpg" alt="testTouchConnectMag">}}
<br />

Success!

[testTouchConnectMag Code](media/testTouchConnectMag.ino)

{{<video2>}}media/testTouchConnectMag.mp4{{</video2>}}
