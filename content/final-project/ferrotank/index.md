+++
title="FerroTank"
lead = "My journey to the final project"
+++
## Iteration 2
{{<image-responsive src="media/final1.jpg" alt="final1">}}
<br />

{{<image-responsive src="media/inside.jpg" alt="inside">}}
<br />

{{<image-responsive src="media/final2.jpg" alt="final2">}}
<br />

##### Download
- [Design file](media/ferrotank_v2_all_design.zip)
- [KiCad](media/kicad_ferrotank_v2.zip)
- [Arduino Code](media/ferrotank_v2_all_code.zip)

## Iteration 1


Ferrotank consists of a multitouch display, electromagnets grid, and a tank with ferrofluid. Ferrofluid is a liquid that can be attracted by the magnetic force. Ferrotank provides an interactive environment to control the shape and flow of ferrofluid.

I documented my journey to make this project in three parts:

- [Multitouch](https://dinany.gitlab.io/fab-academy/final-project/multitouch/)
- [Magnets](https://dinany.gitlab.io/fab-academy/final-project/magnets/)
- [Ferrofluid container](https://dinany.gitlab.io/fab-academy/final-project/ferrofluid-container/)
