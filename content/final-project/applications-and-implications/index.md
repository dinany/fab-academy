+++
title="Applications and Implications"
lead = "Propose a final project masterpiece that integrates the range of units covered"
+++

##### Requirements:

- Your project should incorporate 2D and 3D design, additive and subtractive fabrication processes, electronics design and production, embedded microcontroller interfacing and programming, system integration and packaging

- Where possible, you should make rather than buy the parts of your project

- Projects can be separate or joint, but need to show individual mastery of the skills, and be independently operable

# What will it do?

It's ferrofluid display with capacitive touch layer in the front, and magnets in the back. In between, there is a tank made of glass. The idea is that ferrofluid will follow user's finger position on the touch panel.

# Who's done what beforehand?

Multitouch:

- [Matthew Keeter](http://fab.cba.mit.edu/classes/863.11/people/matthew.keeter/multitouch/index.html)
- [Matt Blackshaw](http://fab.cba.mit.edu/classes/863.10/people/matt.blackshaw/week8.html)

Ferrofluid display:

- [Fetch](https://www.hackster.io/AppliedProc/fetch-a-ferrofluid-display-ca8557)

# What will you design?

I will design

- the step response board for capacitive touch sensor
- magnet board to control the state of magnets
- self-made electromagnets
- glass tank
- 3D model of enclosure

# What materials and components will be used?

- PCB
- Coils for electromagnets
- Nails for electromagnets
- Glass
- PLA for 3D printing
- Ferrofluid

# Where will come from?

- Coils are from Digikey
- Ferrofluid are from EFH-1
- Glass is from a broken scanner

# How much will they cost?

- Coils
- Ferrofluid

# What parts and systems will be made?

# What processes will be used?

- PCB design in Kicad
- PCB milling with MDX-30
- CAD Design in Fusion for the enclosure
- 3D printing
- Coding in Arduino

# What questions need to be answered?

# How will it be evaluated?
