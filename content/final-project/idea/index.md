+++
title="Final Project Idea"
lead = "My journey to the final project"
+++

## Idea 1:

I would like to make a ferrofluid display that can be interacted with fingers. For example, ferrofluid would move to and stay at the position where our finger points. I really like the motion and visual created by ferrofluid. They are organic, unpredictable and dynamic. So I hope to explore the possibilites of ferrofluid in digital fabrication in the final project. I plan to make an interactive artwork for anyone who is interested to play. I need to do a bit more research in order to finalize my idea.

Inspiration:

{{< youtube dOCGQAXV9FY >}}
<br>

## Idea 2:

I would like to build a snowboard with LED embeded. LED strips could be controlled by accelerometer on the snowboard. Different movements could trigger certain visual effects. The user of this snowboard would be myself and I plan to tailer the flex, shape and length most fit my riding style.

{{<image-responsive src="media/snowboard_sketch_resize.jpg" alt="Snowboard Sketch">}}
<br />

In the end, I decided to go with the first idea.
