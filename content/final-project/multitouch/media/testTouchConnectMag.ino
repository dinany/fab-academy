#include <SoftwareSerial.h>

//#define analog_pin 2 //  PA1 of the ATtiny412 -SENSE
//#define tx_pin 3  //     PA2 of the ATtiny412 -STEP

#define sense1 9 //  PA2 of the ATtiny1614 -SENSE1
#define sense2 10 //  PA3 of the ATtiny1614 -SENSE2
#define sense3 8 //  PA1 of the ATtiny1614 -SENSE3
#define sense4 1 //  PA5 of the ATtiny1614 -SENSE4
#define sense5 3 //  PA7 of the ATtiny1614 -SENSE5
#define sense6 2 //  PA6 of the ATtiny1614 -SENSE6

#define STEP 0  //     PA4 of the ATtiny1614 -STEP

#define SENSENUMBER 3


long result1,result2,result3;   //variable for the result of the tx_rx measurement.
long result[SENSENUMBER];

SoftwareSerial mySerial(6,7); //RX_MAG TX_MAG PB1,PB0 of ATtiny1614

void setup() {
pinMode(STEP,OUTPUT);      // provides the voltage step
Serial.begin(115200);

mySerial.begin(115200);  // set the data rate for the SoftwareSerial port

}


long tx_rx(int sense){         //Function to execute rx_tx algorithm and return a value
                      //that depends on coupling of two electrodes.
                      //Value returned is a long integer.
  int read_high;
  int read_low;
  int diff;
  long int sum;
  int N_samples = 100;    //Number of samples to take.  Larger number slows it down, but reduces scatter.

  sum = 0;

  for (int i = 0; i < N_samples; i++){
   digitalWrite(STEP,HIGH);              //Step the voltage high on conductor 1.
   read_high = analogRead(sense);        //Measure response of conductor 2.
   delayMicroseconds(100);            //Delay to reach steady state.
   digitalWrite(STEP,LOW);               //Step the voltage to zero on conductor 1.
   read_low = analogRead(sense);         //Measure response of conductor 2.
   diff = read_high - read_low;       //desired answer is the difference between high and low.
   sum += diff;                       //Sums up N_samples of these measurements.
 }
  return sum;
}                         //End of tx_rx function.


void loop() {
  result[0]= tx_rx(sense3);
  result[1]= tx_rx(sense2);
  result[2]= tx_rx(sense1);
  //result[3]= tx_rx(sense4);
  //result[4]= tx_rx(sense5);
  //result[5]= tx_rx(sense6);
  
  
  for(int n=0; n<SENSENUMBER; n++)
  {
  //  result[n]=map(result[n], 0, 400, 0, 1000);
    Serial.print(result[n]);
    Serial.write('\t');
  }
  
  Serial.println("\n");

  if(result[0]>100){
    mySerial.write(192);//1100 0000
    //mySerial.write(64); //0100 0000
  }
  else{
    mySerial.write(128);//1000 0000
    //mySerial.write(0); //0000 0000

  }
  
  delayMicroseconds(100);
}
