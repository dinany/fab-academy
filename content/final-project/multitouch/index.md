+++
title="Multitouch"
lead = "My journey to the final project"
+++

In the input devices week, I experimented with the [step response](https://dinany.gitlab.io/fab-academy/assignments/input-devices/).

### Multitouch Board

To design a multitouch panel, I learned from [Matt Blackshaw](http://fab.cba.mit.edu/classes/863.10/people/matt.blackshaw/week8.html). Schematic design for 18 resolutions multitouch step-response board:

{{<image-responsive src="media/sch.jpg" alt="schematic">}}
<br />

##### Components:

- ATtiny1614
- UPDI
- 2X2 header x4
- Capacitor 1uF
- LED
- R 1K
- R 1M x12
- FTDI

I milled and stuffed the board.

{{<image-responsive src="media/touchBoard18Res.jpg" alt="touchBoard18Res">}}
<br />

### Touchpad with Copper

I drew the touchpad in Illustrator and used vinyl cutter to cut out the copper sticker.

{{<image-responsive src="media/ai.jpg" alt="ai">}}
<br />

But the outcome was quite rough at the beginning because the blade was almost done.

{{<image-responsive src="media/waste.jpg" alt="waste">}}
<br />

Here is what I got for the first version.

{{<image-responsive src="media/roughcut.jpg" alt="roughcut">}}
<br />

Ideally, three pads connected to the same pin should give different range of values when I touch so that finger can be located precisely. However, they provided similar values. I assume it was caused by noises.

{{<video>}}media/1stDemo.mp4{{</video>}}
<br />

Later, Kris replaced the blade and suggested to replace the cardboard with epoxy layer to cancel some noises.

{{<image-responsive src="media/sandwich.jpg" alt="sandwich">}}
{{<image-responsive src="media/finecut.jpg" alt="finecut">}}
<br />

Setting for the vinyl cutter:

- force: 80gf
- speed: 2cm/s

But still three pads connected to the same pin gave similar values. Plan B is to make each pin connect to single pad and make 3x3 grids.

18-resolution board can still be used as a test board to send message to the magnet board.

{{<image-responsive src="media/testTouchConnectMag.jpg" alt="testTouchConnectMag">}}
<br />

Test to communicate with the magnet board. Success!

[testTouchConnectMag Code](media/testTouchConnectMag.ino)

{{<video2>}}media/testTouchConnectMag.mp4{{</video2>}}

Finally I figuired out the touch board is actually working. It was the way of interaction matters. In order to generate different signals, the pads on both layers needs to be touched at the same time.

{{<video>}}media/copper.mp4{{</video >}}
<br />

### ITO film

ITO stands for indium tin oxide. It is transparent and electrically conductive, which fits my project perfectly. Matti helped me order the [ITO film](https://www.adafruit.com/product/1309).

Next is to test the power and speed value to engrave and cut the ITO.

If the engraving is too light, it won't cut off the continuity between pads. If the engraving is too much, it will leave visible marks.

{{<image-responsive src="media/ito_engrave_test.jpg" alt="ito_engrave_test">}}
<br />

##### Engrave:

| Power | Speed | Thickness |   Outcome |
| ----- | :---: | --------: | --------: |
| 100   |  20   |       1mm |  too deep |
| 100   |  60   |       1mm |  too deep |
| 100   |   5   |    0.05mm | too light |
| 100   |  10   |     0.1mm |        OK |
| 100   |   8   |     0.1mm | too light |

##### Cut

| Power | Speed | Frequency | Outcome |
| ----- | :---: | --------: | ------: |
| 100   |   8   |       500 |       x |
| 100   |  20   |       500 |      ok |

{{<video>}}media/ito_engrave.mp4{{</video >}}
<br />

Here is what I got.

{{<image-responsive src="media/ito_pads.jpg" alt="ito_pads">}}
<br />

Since ITO is not able to be soldered, I cut out FR-4 board and used clips to clamp them together.

{{<image-responsive src="media/clamp.jpg" alt="clamp">}}
<br />

Test with the board.

{{<video>}}media/ito.mp4{{</video >}}
<br />

I fixed ITO layers on top of a piece of acrylic using tapes as below.

{{<image-responsive src="media/ito_on_acrylic.jpg" alt="ito_on_acrylic.jpg">}}
<br />

Up to this point, i realized that i didn't have to cut them out. Engraving instead would give same result and easier to bond two layers together.

### Download

- Arduino Code for [Multitouch](media/step_multiple_revise_11_5.ino)
- ai file [Touch pads](media/touchpads.zip)
- kicad [Mutitouch PCB](media/step-response-multiple.zip)
