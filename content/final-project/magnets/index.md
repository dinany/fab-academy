+++
title="Magnets"
lead = "My journey to the final project"
+++

In the output devices week, I played around with [magnets](https://dinany.gitlab.io/fab-academy/assignments/output-devices/) and made a board that controls one single magnet.

For my final project, I designed the pcb board so that it can manipulate up to 9 magnets at the same time.

{{<image-responsive src="media/schematic.jpg" alt="schematic.jpg">}}
<br />

{{<image-responsive src="media/trace.jpg" alt="trace.jpg">}}
<br />

{{<image-responsive src="media/pcb.jpg" alt="pcb.jpg">}}
<br />

My first-version container uses acrylic as my tank. Here is my design in Fusion.

{{<fusion src="https://aalto180.autodesk360.com/g/shares/SH9285eQTcf875d3c5397a8fc05403119a74">}}

Before I loaded the ferrotank, I tested if the magnets working as expected. Visuals are created in Processing and each square corresponds to one magnet. When the mouse hovers on the square and turn it to green, the magnet at the same location will be switched on.

{{<video>}}media/testMag.mp4{{</video >}}
<br />

Although the magnets worked, acrylic tank was not good since ferrofluid tended to stick onto the acylic surface and left messy traces.

So I decided to use commerical glass container before I found a solution.

I also programmed the board so that the magnets are switched on and off in a fixed pattern.

{{<video>}}media/pattern.mp4{{</video >}}
<br />

### Download

- Arduino Code for [magnetx9](media/magnetx9.ino)
- Processing Code for [magnetx9](media/sketch_3x3.zip)
- Arduino Code for [pattern](media/pattern.ino)
- kicad [Magnet PCB](media/magnet-final.zip)
