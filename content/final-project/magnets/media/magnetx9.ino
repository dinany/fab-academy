//const int Gate1 = 8; //  PA1 of the ATtiny1614
//const int Gate2 = 9; //  PA2 of the ATtiny1614
//const int Gate3 = 10; //  PA3 of the ATtiny1614
//const int Gate4 = 0; //  PA4 of the ATtiny1614
//const int Gate5 = 1; //  PA5 of the ATtiny1614
//const int Gate6 = 2; //  PA6 of the ATtiny1614
//const int Gate7 = 3; //  PA7 of the ATtiny1614
//const int Gate8 = 7; //  PB0 of the ATtiny1614
//const int Gate9 = 6; //  PB1 of the ATtiny1614

int Gate[] = {8,9,10,0,1,2,3,7,6};

byte incomingByte;
long magnetOnMaxMs = 2000; // The maximum time the magnet can be on
long magnetOnTimeMs[9]; // To be set when we turn the magnet on
long magnetOnDeltaMs[9];

int temp;


void setup() {
  
  for(int i=0; i<9; i++)
  {
    pinMode(Gate[i], OUTPUT);
  }

  Serial.begin(115200);
}

void loop() {
  // 0001 1
  // 0010 2 x
  // 0011 3
  // 0100 4 x
  // 0101 5
  // 0110 6 x
  // 0111 7
  // 1000 8
  // 1001 9

  while (Serial.available() > 0) {
    incomingByte = Serial.read();
    int target = (incomingByte & (15 << 4)) >> 4;  //15 = 00001111 << 4 => 11110000
//  int state = (incomingByte & (1 << 3 )) >> 3; //00000001 << 3 => 00001000

    for(int i=0; i<9; i++)
    {
      if(target == (i+1))
      {
         digitalWrite(Gate[i], HIGH);
         magnetOnTimeMs[i] = millis(); // capture the current time        
      }
    }

    
  }

  for(int i=0; i<9; i++)
  {
    magnetOnDeltaMs[i]= millis() - magnetOnTimeMs[i]; // Measure time since we turned the magnet on
    if(magnetOnDeltaMs[i] > magnetOnMaxMs)
    {
      digitalWrite(Gate[i], LOW);
    }
  }
}
