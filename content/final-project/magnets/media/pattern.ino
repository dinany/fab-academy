//const int Gate0 = 8; //  PA1 of the ATtiny1614
//const int Gate1 = 9; //  PA2 of the ATtiny1614
//const int Gate2 = 10; //  PA3 of the ATtiny1614
//const int Gate3 = 0; //  PA4 of the ATtiny1614
//const int Gate4 = 1; //  PA5 of the ATtiny1614
//const int Gate5 = 2; //  PA6 of the ATtiny1614
//const int Gate6 = 3; //  PA7 of the ATtiny1614
//const int Gate7 = 7; //  PB0 of the ATtiny1614
//const int Gate8 = 6; //  PB1 of the ATtiny1614

//0 1 2 
//3 4 5
//6 7 8
//int Gate[] = {8,9,10,0,1,2,3,7,6};

//5 6 7
// 4 3 
//0 1 2
int Gate[] = {3,7,6,2,1,8,3,7}; //only 8 electromagnets in use now with diagonal design

//byte incomingByte;
long magnetOnMaxMs = 2000; // The maximum time the magnet can be on
long magnetOnTimeMs[8]; // To be set when we turn the magnet on
long magnetOnDeltaMs[8];
int onOrOff[]={0,0,0,0,0,0,0,0};//0 off, 1 on
int count=0;
int mode=0; //0:0->7; 1:7->0

//int temp;


void setup() {
  
  for(int i=0; i<8; i++)
  {
    pinMode(Gate[i], OUTPUT);
  }

  //Serial.begin(115200);

  digitalWrite(Gate[0],HIGH);
  magnetOnTimeMs[0] = millis();
  onOrOff[0]=1;
}

void loop() {
  // 0001 1
  // 0010 2 x
  // 0011 3
  // 0100 4 x
  // 0101 5
  // 0110 6 x
  // 0111 7
  // 1000 8
  // 1001 9

  if(mode ==0)
  {
    for(int i=0; i<7; i++)
    {
      magnetOnDeltaMs[i]= millis() - magnetOnTimeMs[i]; // Measure time since we turned the magnet on
      if(magnetOnDeltaMs[i] > magnetOnMaxMs && onOrOff[i]==1)
      {
        digitalWrite(Gate[i], LOW);
        onOrOff[i]=0;
        digitalWrite(Gate[i+1],HIGH);
        onOrOff[i+1]=1;
        magnetOnTimeMs[i+1] = millis();
        count++;
      }
    }
  }
  else //mode ==1
  {
    for(int i=7;i>0;i--)
    {
      magnetOnDeltaMs[i]= millis() - magnetOnTimeMs[i]; // Measure time since we turned the magnet on
      if(magnetOnDeltaMs[i] > magnetOnMaxMs && onOrOff[i]==1)
      {
        digitalWrite(Gate[i], LOW);
        onOrOff[i]=0;
        digitalWrite(Gate[i-1],HIGH);
        onOrOff[i-1]=1;
        magnetOnTimeMs[i-1] = millis();
        count--;
      }
    }
  }
    
  if(count == 7)
    mode =1;
  
  
  if(count==0){
      mode=0;
  }
  
  
}
