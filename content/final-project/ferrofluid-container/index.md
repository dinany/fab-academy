+++
title="Ferrofluid"
lead = "My journey to the final project"
+++

In what environment can ferrofluid move smoothly? Karl helped me figure out the components of Matti's sample. The transparent liquid around the ferrofluid is composed of water, salt, and saccharose. Its high conductivity comes from salt. Most likely the sugar is added for the density. And the density is 1.1g/ml.

So I mixed water and salt in the ratio 10:1 for the first try.

Here is what I got:

{{<video2>}}media/firstTry.mp4{{</video2>}}

There were some ferrofluid "dead" floating on the surface. It might because the salt didn't fully dissolve in the water and trapped those ferros.

Also the material of the container matters. In this case, I used the plastic cups from Jungle Juice (Collagen Builder is the best! Also love Limely Lemon!). Part of the ferrofluid was stick and stayed on the wall when moving. I'll try glass next time.
