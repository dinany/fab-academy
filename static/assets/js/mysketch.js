var w = window.innerWidth;
var h = window.innerHeight/3;
var count=0;

function setup() {
    var myCanvas = createCanvas(w, h);
    myCanvas.class('backgroundsketch');
}

function draw() {
    if(frameCount%20 && count<300){
        var r=random(10,80);
        var a=(10,100);
        noStroke();
        fill(200,110,20,r);
        ellipse(random(w),random(h),r,r);
        count++;
    }
}

function windowResized() {
    resizeCanvas(window.innerWidth, window.innerHeight);
 }

//  function mouseWheel() {
//     resizeCanvas(window.innerWidth, window.innerHeight);
//   }
